<?php namespace Jambolo\Excelbulkimports\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloExcelbulkimportsImports extends Migration
{
    public function up()
    {
        Schema::create('jambolo_excelbulkimports_imports', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('path')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_excelbulkimports_imports');
    }
}
