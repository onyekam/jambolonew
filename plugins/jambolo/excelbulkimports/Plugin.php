<?php namespace Jambolo\Excelbulkimports;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
            return [
                'Jambolo\Excelbulkimports\Components\BulkUpload' => 'Bulkupload'
            ];
    }

    public function registerSettings()
    {
    }
}
