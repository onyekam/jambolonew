<?php namespace Jambolo\Excelbulkimports\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Jambolo\Artisans\Models\Association;
use Jambolo\Artisans\Models\ArtisanCategory as Category;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\ExcelBulkImports\Models\Import;
use Redirect; 
use Session;
use Request;
use Vdomah\Excel\Classes\Excel;
use BackendAuth;

class BulkUpload extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Test Bulk Upload',
			'description' => 'Test Bulk Upload'
		];
	}



	public function onRun(){
        $this->backendUser = BackendAuth::getUser();
    }

    public function onUpload(){
        $import = new Import;
        $data = Input::file('upload');
        //dd($data);
        $import->upload = $data;
        $import->save();
        $path = $import->upload->getDiskPath();
        //dd($path);
        //dd(base_path());
        //$path = str_replace(Request::getBaseUrl(),'',$path);
        //dd($path);
        return $path;
    }

    public function runBulkUpload(){
        $path = $this->onUpload();
        $data = Excel::excel()->load('storage/app/'.$path, function($reader) {})->first();
        if (!empty($data) && $data->count()) {
        $headers = $data->first()->keys()->toArray();    
        //var_dump($headers);
        foreach ($data as $key => $value ) {
            //echo $value->name .' | '. $value->company_name . ' | ' . $value->telephone_number . ' | ' . $value->address . ' | ' . $value->lga . ' | ' . $value->category . ' | ' . $value->association . ' | ' . $value->area . ' | ' . $value->premium . ' | ' . $value->longitude . ' | ' . $value->latitude . ' | ' . $value->city .'<br/><br/>';
            $existingArtisan = Artisan::where('slug', $this->to_permalink($value->company_name))->first();
            /* Update Exisiting Artisans */
            if ($existingArtisan) {
                $existingArtisan->first_name = $value->name;
                $existingArtisan->last_name = '';
                $existingArtisan->slug = $this->to_permalink($value->company_name);
                $existingArtisan->address = $value->address;
                
                $existingArtisan->phone_number = $value->telephone_number;
                $city = City::where( 'slug',$value->city)->first();
                $existingArtisan->city_id = $city['id'];
                $area = Area::where( 'slug',$value->area)->first();
                $existingArtisan->area_id = $area['id'];
                $category = Category::where( 'slug' , $value->category )->first();
                $existingArtisan->category_id = $category['id'];
                //echo "area" .$existingArtisan->area_id;
                $existingArtisan->longitude = $value->longitude;
                $existingArtisan->latitude = $value->latitude;
                if ($value->association != "Not Applicable") {
                    $association = Association::where('slug', $value->association)->first();
                    $existingArtisan->association_id = $association['id'];
                }
                $existingArtisan->company_name = $value->company_name;
                if ( $value->subscription == FALSE ) {
                    $existingArtisan->premium = 0;
                } else if ( $value->subscription == TRUE ) {
                    $existingArtisan->premium = 1;
                    $existingArtisan->email = $value->email;
                }
                $existingArtisan->save();
                //continue;
            } else {
                $artisan = new Artisan;
                $artisan->first_name = $value->name;
                $artisan->last_name = '';
                $artisan->slug = $this->to_permalink($value->company_name);
                $artisan->address = $value->address;
                $artisan->phone_number = $value->telephone_number;
                $city = City::where( 'slug',$value->city)->first();
                $artisan->city_id = $city['id'];
                $area = Area::where( 'slug',$value->area)->first();
                $artisan->area_id = $area['id'];
                $category = Category::where( 'slug' , $value->category )->first();
                $artisan->category_id = $category['id'];
                //echo "area" .$artisan->area_id;
                $artisan->longitude = $value->longitude;
                $artisan->latitude = $value->latitude;
                if ($value->association != "Not Applicable") {
                    $association = Association::where('slug', $value->association)->first();
                    $artisan->association_id = $association['id'];
                }
                $artisan->company_name = $value->company_name;
                if ( $value->subscription == FALSE ) {
                    $artisan->premium = 0;
                } else if ( $value->subscription == TRUE ) {
                    $artisan->premium = 1;
                    $artisan->email = $value->email;
                }
                $artisan->save();

            }
        }       
        
       }

       return Redirect::refresh();
    }

    public function to_permalink($str){
		if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
			$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		$str = strtolower( trim($str, '-') );
		return $str;
    }

    public function onBulkExport(){
        $artisans = Artisan::get();
        //dd($artisans);
        $arti = array();
        $artisans2 = array();
        foreach ($artisans as $artisan) {
            // if ($artisan->state_id != 1) {
            //     dd($artisan->company_name." ".$artisan->first_name." ".$artisan->last_name." ".$artisan->area_id);
            // }
            $arti['first_name'] = $artisan->first_name;
            $arti['last_name'] = $artisan->last_name;
            $arti['company_name'] = $artisan->company_name;
            $arti['phone_number'] = $artisan->phone_number;
            $arti['category'] = $artisan->category->slug;
            $arti['state'] = $artisan->state->slug;
            $arti['city'] = $artisan->city->slug;
            $arti['area'] = $artisan->area->slug;
            $arti['address'] = $artisan->address;
            $arti['latitude'] = $artisan->latitude;
            $arti['longitude'] = $artisan->longitude;
            $arti['premium'] = $artisan->premium;
            array_push($artisans2, $arti);
        }
        Excel::excel()->create('artisans3', function($excel) use ($artisans2) {
            $excel->sheet('Artisans', function($sheet) use ($artisans2) {
                $sheet->fromArray($artisans2);
            });
        })->store('xls');
    }

    public function onBulkEmailExport(){
        $users = User::get();
        //dd($artisans);
        $userArray = array();
        $usersArray = array();
        foreach ($users as $user) {
            // if ($artisan->state_id != 1) {
            //     dd($artisan->company_name." ".$artisan->first_name." ".$artisan->last_name." ".$artisan->area_id);
            // }
            // $arti['first_name'] = $artisan->first_name;
            // $arti['last_name'] = $artisan->last_name;
            $userArray['email'] = $user->email;
            // $arti['phone_number'] = $artisan->phone_number;
            // $arti['category'] = $artisan->category->slug;
            // $arti['state'] = $artisan->state->slug;
            // $arti['city'] = $artisan->city->slug;
            // $arti['area'] = $artisan->area->slug;
            // $arti['address'] = $artisan->address;
            // $arti['latitude'] = $artisan->latitude;
            // $arti['longitude'] = $artisan->longitude;
            // $arti['premium'] = $artisan->premium;
            array_push($usersArray, $userArray);
        }
        Excel::excel()->create('artisans3', function($excel) use ($usersArray) {
            $excel->sheet('Artisans', function($sheet) use ($usersArray) {
                $sheet->fromArray($usersArray);
            });
        })->store('xls');
    }


    public function onChunkedExport() {
        $artisans = Artisan::take(5)->get();
        $arti = array();
        $artisans2 = array();
        foreach ($artisans as $artisan) {
            $arti['first_name'] = $artisan->first_name;
            $arti['last_name'] = $artisan->last_name;
            $arti['company_name'] = $artisan->company_name;
            $arti['phone_number'] = $artisan->phone_number;
            $arti['category'] = $artisan->category->category_name;
            $arti['state'] = $artisan->state->state_name;
            $arti['city'] = $artisan->city->city_name;
            $arti['area'] = $artisan->area->area_name;
            $arti['address'] = $artisan->address;
            $arti['latitude'] = $artisan->latitude;
            $arti['longitude'] = $artisan->longitude;
            $arti['premium'] = $artisan->premium;
            array_push($artisans2, $arti);
        }
        Excel::excel()->create('artisanschunked', function($excel) use ($artisans2) {
        $excel->sheet('Artisans', function($sheet) use($artisans2) {
            $sheet->appendRow(array(
                'first_name', 'last_name', 'company_name', 'phone_number', 'category', 'state', 'city', 'area', 'address', 'latitude', 'longitude', 'premium'
            ));
            $artisans2->chunk(100, function($rows) use ($sheet)
            {
                foreach ($rows as $row)
                {
                    $sheet->appendRow(array(
                        $row->first_name, $row->last_name, $row->company_name, $row->phone_number, $row->category, $row->state, $row->city, $row->area, $row->address, $row->latitude, $row->longitude, $row->premium
                    ));
                }
            });
        });
    })->store('xlsx');
    }
    
    public $backendUser;
    public $artisans;
}