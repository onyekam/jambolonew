<?php namespace Jambolo\Excelbulkimports\Models;

use Model;

/**
 * Model
 */
class Import extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_excelbulkimports_imports';

    public $attachOne = [
        'upload' => 'System\Models\File'
    ];
}