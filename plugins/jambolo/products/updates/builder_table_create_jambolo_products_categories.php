<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsCategories extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('category_name')->nullable();
            $table->string('slug');
            $table->integer('parent_category')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_categories');
    }
}
