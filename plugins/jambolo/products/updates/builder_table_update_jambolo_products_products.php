<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsProducts extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_products', function($table)
        {
            $table->string('product_excerpt')->nullable();
            $table->text('product_description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_products', function($table)
        {
            $table->dropColumn('product_excerpt');
            $table->dropColumn('product_description');
        });
    }
}
