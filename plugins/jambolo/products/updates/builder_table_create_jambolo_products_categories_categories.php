<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsCategoriesCategories extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_categories_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('child_category_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_categories_categories');
    }
}
