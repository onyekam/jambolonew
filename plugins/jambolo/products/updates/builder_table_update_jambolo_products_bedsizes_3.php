<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsBedsizes3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_bedsizes', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_bedsizes', function($table)
        {
            $table->smallInteger('id')->change();
        });
    }
}
