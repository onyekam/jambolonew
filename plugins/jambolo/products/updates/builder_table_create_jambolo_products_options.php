<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsOptions extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_options', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('bedsize')->nullable();
            $table->string('shoesize')->nullable();
            $table->string('bedfloor')->nullable();
            $table->integer('drawer')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_options');
    }
}
