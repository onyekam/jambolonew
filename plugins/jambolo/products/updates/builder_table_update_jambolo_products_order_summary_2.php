<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOrderSummary2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_order_summary', function($table)
        {
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_order_summary', function($table)
        {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('email');
        });
    }
}
