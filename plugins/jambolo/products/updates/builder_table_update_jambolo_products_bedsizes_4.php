<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsBedsizes4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_bedsizes', function($table)
        {
            $table->decimal('cost', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_bedsizes', function($table)
        {
            $table->dropColumn('cost');
        });
    }
}
