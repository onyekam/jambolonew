<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsShoesizes extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_shoesizes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('size');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_shoesizes');
    }
}
