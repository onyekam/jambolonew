<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsAttributes extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_attributes', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_attributes', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
