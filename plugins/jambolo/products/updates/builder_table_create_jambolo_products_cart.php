<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsCart extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_cart', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('cart_cookie')->nullable();
            $table->integer('quantity')->nullable();
            $table->double('cost', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_cart');
    }
}
