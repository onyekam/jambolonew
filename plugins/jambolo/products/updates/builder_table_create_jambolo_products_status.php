<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsStatus extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_status');
    }
}
