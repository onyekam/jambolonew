<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOrderSummary extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_order_summary', function($table)
        {
            $table->dropColumn('delivery_terms_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_order_summary', function($table)
        {
            $table->integer('delivery_terms_id')->nullable();
        });
    }
}
