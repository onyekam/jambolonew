<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOrderDetails2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_order_details', function($table)
        {
            $table->renameColumn('size', 'itemsize');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_order_details', function($table)
        {
            $table->renameColumn('itemsize', 'size');
        });
    }
}
