<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsCart2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_cart', function($table)
        {
            $table->string('size')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_cart', function($table)
        {
            $table->dropColumn('size');
        });
    }
}
