<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsAttributes extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_attributes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('quantity')->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->integer('product_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_attributes');
    }
}
