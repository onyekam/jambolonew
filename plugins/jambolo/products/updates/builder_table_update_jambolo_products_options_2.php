<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOptions2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_options', function($table)
        {
            $table->decimal('cost', 10, 0);
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_options', function($table)
        {
            $table->dropColumn('cost');
        });
    }
}
