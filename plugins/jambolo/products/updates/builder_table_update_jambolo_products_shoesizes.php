<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsShoesizes extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_shoesizes', function($table)
        {
            $table->string('size', 10)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_shoesizes', function($table)
        {
            $table->integer('size')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
