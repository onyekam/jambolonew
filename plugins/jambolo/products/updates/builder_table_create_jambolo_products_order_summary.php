<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsOrderSummary extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_order_summary', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->double('total', 10, 0)->nullable();
            $table->integer('payment_method_id')->nullable();
            $table->integer('delivery_terms_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('city')->nullable();
            $table->string('transref')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_order_summary');
    }
}
