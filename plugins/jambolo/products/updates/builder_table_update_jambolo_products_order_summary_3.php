<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOrderSummary3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_order_summary', function($table)
        {
            $table->double('vat_amount', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_order_summary', function($table)
        {
            $table->dropColumn('vat_amount');
        });
    }
}
