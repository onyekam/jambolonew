<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsCart3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_cart', function($table)
        {
            $table->text('item_options')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_cart', function($table)
        {
            $table->dropColumn('item_options');
        });
    }
}
