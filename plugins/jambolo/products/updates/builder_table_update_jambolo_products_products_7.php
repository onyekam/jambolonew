<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsProducts7 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_products', function($table)
        {
            $table->integer('new')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_products', function($table)
        {
            $table->dropColumn('new');
        });
    }
}
