<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsCategories2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_categories', function($table)
        {
            $table->renameColumn('parent_category_id', 'category_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_categories', function($table)
        {
            $table->renameColumn('category_id', 'parent_category_id');
        });
    }
}
