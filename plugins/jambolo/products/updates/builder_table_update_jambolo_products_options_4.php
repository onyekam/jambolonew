<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOptions4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_options', function($table)
        {
            $table->integer('drawer')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_options', function($table)
        {
            $table->integer('drawer')->default(null)->change();
        });
    }
}
