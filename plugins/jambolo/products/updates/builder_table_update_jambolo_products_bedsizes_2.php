<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsBedsizes2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_bedsizes', function($table)
        {
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_bedsizes', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
