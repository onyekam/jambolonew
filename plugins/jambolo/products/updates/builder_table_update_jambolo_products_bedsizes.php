<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsBedsizes extends Migration
{
    public function up()
    {
        Schema::rename('jambolo_products_bedsize', 'jambolo_products_bedsizes');
    }
    
    public function down()
    {
        Schema::rename('jambolo_products_bedsizes', 'jambolo_products_bedsize');
    }
}
