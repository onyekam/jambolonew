<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsProducts extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->decimal('price', 10, 0);
            $table->integer('artisan_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_products');
    }
}
