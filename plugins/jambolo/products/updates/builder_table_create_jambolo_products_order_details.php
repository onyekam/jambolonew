<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsOrderDetails extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_order_details', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->integer('quantity');
            $table->double('price', 10, 0)->nullable();
            $table->double('total', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_order_details');
    }
}
