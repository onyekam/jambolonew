<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsProducts6 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_products', function($table)
        {
            $table->string('wood_type')->nullable();
            $table->string('bed_floor')->nullable();
            $table->string('pillows')->nullable();
            $table->string('bedside_drawer')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_products', function($table)
        {
            $table->dropColumn('wood_type');
            $table->dropColumn('bed_floor');
            $table->dropColumn('pillows');
            $table->dropColumn('bedside_drawer');
        });
    }
}
