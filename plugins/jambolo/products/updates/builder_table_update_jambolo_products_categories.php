<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsCategories extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_categories', function($table)
        {
            $table->renameColumn('parent_category', 'parent_category_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_categories', function($table)
        {
            $table->renameColumn('parent_category_id', 'parent_category');
        });
    }
}
