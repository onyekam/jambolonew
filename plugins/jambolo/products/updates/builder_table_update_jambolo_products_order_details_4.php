<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloProductsOrderDetails4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_products_order_details', function($table)
        {
            $table->integer('artisan_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_products_order_details', function($table)
        {
            $table->dropColumn('artisan_id');
        });
    }
}
