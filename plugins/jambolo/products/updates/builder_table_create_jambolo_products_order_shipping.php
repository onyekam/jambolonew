<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsOrderShipping extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_order_shipping', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->smallInteger('order_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('phone_number');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_order_shipping');
    }
}
