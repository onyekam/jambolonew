<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsAttributeValueProductAttributeValue extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_attribute_value_product_attribute_value', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('attribute_value_id')->nullable()->unsigned();
            $table->integer('product_attribute_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_attribute_value_product_attribute_value');
    }
}
