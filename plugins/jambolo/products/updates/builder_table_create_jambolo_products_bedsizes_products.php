<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsBedsizesProducts extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_bedsizes_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->smallInteger('bedsize_id');
            $table->smallInteger('product_id');
            $table->primary(['bedsize_id','product_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_bedsizes_products');
    }
}
