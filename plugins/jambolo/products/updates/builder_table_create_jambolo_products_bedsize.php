<?php namespace Jambolo\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloProductsBedsize extends Migration
{
    public function up()
    {
        Schema::create('jambolo_products_bedsize', function($table)
        {
            $table->engine = 'InnoDB';
            $table->smallInteger('id');
            $table->string('bedsize')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_products_bedsize');
    }
}
