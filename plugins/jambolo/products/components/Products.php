<?php namespace Jambolo\Products\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Jambolo\Products\Models\Manufacturer;
use Jambolo\Products\Models\Product;
use Jambolo\Products\Models\ProductCategory as Category;
use Jambolo\Products\Models\Color;
use Redirect; 
use Illuminate\Support\Collection;
use Auth;
use Flash;
use System\Models\File as File;

class Products extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Shop on Artisan Page',
			'description' => 'List Products on Artisan Page'
		];
	}
	
	public function onRun(){
		$this->products = $this->getMarketplaceProducts();
		$this->featuredProducts = $this->getFeaturedProducts();
		$this->homeProducts = $this->getHomeProducts();
		$this->results = $this->products;
		$this->categories = $this->getCategories();
		$this->recentProducts = $this->getrecentProducts();
		$this->allProducts = $this->getAllProducts();
	}

	public function getMarketplaceProducts(){
		
		$products = Product::all();
		// dd($products);
		return $products;
	}

	public function getHomeProducts(){
		$products = Product::all()->take(8);
		// dd($products);
		return $products;
	}

	public function getrecentProducts(){
		$products = Product::orderBy('created_at', 'desc')->take(4)->get();
		// dd($products);
		return $products;
	}

	public function getFeaturedProducts(){
		$products = Product::where('featured','1')->get();
		// dd($products);
		return $products;
	}

	public function getCategories(){
		$categories = Category::all();
		return $categories;
	}

	public function getAllProducts(){
		if($this->param('category-slug')){
			$category = Category::where('slug', $this->param('category-slug'))->first();
			$products = Product::where('category_id', $category->id)->paginate(9);
		} else {
			$products = Product::paginate(9);
		}
		$this->totalCount = $products->total();
		return $products;
	}

	public $totalCount;
	public $allProducts;
	public $products;
	public $featuredProducts;
	public $homeProducts;
	public $results;
	public $categories;
	public $recentProducts;
}