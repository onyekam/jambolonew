<?php namespace Jambolo\Products\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Jambolo\Products\Models\Order;
use Jambolo\Products\Models\OrderDetails;
use Redirect; 
use Auth;
use Jambolo\Products\Models\Settings;


class DisplayOrderDetails extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Order details',
			'description' => 'List all Order details'
		];
	}



	public function onRun(){
		$this->displayOrderDetails();
		//return $this->callForTenders;
        // if ($this->param('slug')) {
		// 	$this->displayManufacturer();
		// 	$this->displayManufacturerProducts();	
        // } else {
        // 	$this->manufacturers = Manufacturer::all();
        // }
        

    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
	// }
	
	public function displayOrderDetails(){
		$user = Auth::getUser();
		$this->orderDetails = OrderDetails::where('order_id', $this->param('slug'))->get();
		$this->order = Order::where('id',$this->param('slug'))->first();

		//$this->vat = Settings::get('vat_value');
		
    }
	public $orderDetails;
	public $order;
	public $vat;
	
	//public $products;

}