<?php namespace Jambolo\Products\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Jambolo\Artisans\Models\Artisan as Artisan;
use Jambolo\Products\Models\Manufacturer;
use Jambolo\Products\Models\Product;
use Jambolo\Products\Models\Category;
use Jambolo\Products\Models\Color;
use Redirect; 
use Illuminate\Support\Collection;
use Auth;
use Flash;
use System\Models\File as File;
use Session;
use Debugbar;


class DashboardProducts extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Shop',
			'description' => 'List all products'
		];
	}



	public function onRun(){
        $this->product = $this->getProduct();
    }
public function getProduct(){
    $product = Product::where('slug', $this->param('slug'))->first();
    return $product;
}
    /* public function getCategories(){
        $categories = Category::all();
        return $categories;
    } */

   /*  public function getManufacturers(){
        $manufacturers = Manufacturer::all();
        return $manufacturers;
    } */
	public function displayArtisanProducts(){
        //$user = Auth::getUser();
        
        //$productCategory = Category::where('slug', $this->param('slug'))->first();
       
       /*  if(Product::where('artisan_id', $this->loggedInArtisan->id)->get()){
            $products = Product::where('artisan_id', $this->loggedInArtisan->id)->get();
        } else {
            $products = "Artisan doesn't have products for sale";
        }
        return $products; */
		//$products = new Collection;
		
    }
    public function onEditProduct(){
        //$user = Auth::getUser();
        $product = Product::where('slug', $this->param('slug'))->where('artisan_id', Input::get('artisan_id'))->first();
        $product->name = Input::get('product_name');
        $product->price = Input::get('price');
        $product->slug = $this->to_permalink(Input::get('product_name'));
        $product->product_excerpt = Input::get('product_excerpt');
        $product->product_description = Input::get('product_description');
        // $product->category_id = Input::get('category_id');
        // $product->manufacturer_id = Input::get('manufacturer_id');
        // $product->product_reference = Input::get('product_reference');
        // $product->product_sku = Input::get('product_sku');
        $product->artisan_id = Input::get('artisan_id');
        $product->product_image = Input::file('product_image');
        // $product->hover_image = Input::file('hover_image');
        /* if (Input::file('image1')) {
            $product->gallery = nput::file('image1');
        }
        if (Input::file('image2')) {
            $product->gallery = nput::file('image2');
        }
        if (Input::file('image3')) {
            $product->gallery = nput::file('image3');
        }
        if (Input::file('image4')) {
            $product->gallery = nput::file('image4');
        } */
        $product->save();
		Flash::success('Product Successfully Edited!');
        return Redirect::to('/my-account/my-products');
    }

	public function onCreateProduct(){
        $product = new Product;
        $product->name = Input::get('product_name');
        $product->price = Input::get('price');
        $product->slug = $this->to_permalink(Input::get('product_name'));
        $product->product_excerpt = Input::get('product_excerpt');
        $product->product_description = Input::get('product_description');
        // $product->category_id = Input::get('category_id');
        // $product->manufacturer_id = Input::get('manufacturer_id');
        // $product->product_reference = Input::get('product_reference');
        // $product->product_sku = Input::get('product_sku');
        // $product->published = 0;
        $product->artisan_id = Input::get('artisan_id');
        $product->product_image = Input::file('product_image');
        // $product->hover_image = Input::file('hover_image');
        /* if (Input::file('image1')) {
            $product->gallery = nput::file('image1');
        }
        if (Input::file('image2')) {
            $product->gallery = nput::file('image2');
        }
        if (Input::file('image3')) {
            $product->gallery = nput::file('image3');
        }
        if (Input::file('image4')) {
            $product->gallery = nput::file('image4');
        } */
        $product->save();
        /* $colors = post('color_id');
        for ($i=0; $i < count($colors); $i++) { 
            DB::statement('INSERT INTO ffande_procurement_colors_products (product_id, color_id) VALUES ('.$product->id, $colors[$i].')');
        } */
        /* $categories = post('category_id');
        for ($i=0; $i < count($categories); $i++) { 
            DB::statement('INSERT INTO ffande_procurement_categories_products (product_id, category_id) VALUES ('.$product->id, $colors[$i].')');
        } */
		Flash::success('Product Successfully Created!');
        return Redirect::refresh();
    }
    // public function onGalleryUpload(){
    //     $image = Input::all();
    //     $images = [];
    //     $print_images = "";
    //     foreach ($image['gallery'] as $printImageFromInput) {
	// 		array_push($images, (new File())->fromPost($printImageFromInput));
    //     }
    //     foreach ($images as $print_image){
    //         $print_images .= '<img src="'. $print_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
    //     }
    //     return [
    //         '#gallery' => $print_images
    //     ];
    // }

    public function onImageOne(){
        $image = Input::all();
        //$images = [];
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image1']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image1' => $product_image
        ];
    }
    public function onImage2(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image2']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image2' => $product_image
        ];
    }
    public function onImage3(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image3']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image3' => $product_image
        ];
    }
    public function onImage4(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['image4']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image4' => $product_image
        ];
    }

    public function onProductImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['product_image']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image' => $product_image
        ];
    }

    public function onPrintImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $product_image = (new File())->fromPost($image['product_image']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $product_image = '<img src="'.$product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#product_image' => $product_image
        ];
    }

    public function onHoverImageUpload(){
        $image = Input::all();
        //$images = [];
        $project_images = "";
        // foreach ($image['project_images'] as $projectImage) {
		// 	array_push($images, (new File())->fromPost($projectImage));
        // }
        $hover_image = (new File())->fromPost($image['hover_image']);
        // foreach ($images as $product_image){
        //     $product_images .= '<img src="'. $product_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        // }
        $hover_image = '<img src="'.$hover_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#hover_image' => $hover_image
        ];
    }
    

    public function onDeleteProduct(){
        $product = Product::find(post('product'))->first();
        $product->product_image->delete();
        $product->delete();
    }

    public function onProductImageDelete(){
        //Debugbar::info(post('product'));
        $product = Product::where('id',post('product'))->first();
        //Debugbar::info($product->product_image);
        //dd();
        $product->product_image->delete();
        $product_image = "Product Image Preview";
        //$product->save();
        return [
            '#product_image' => $product_image
        ];
        
		//Debugbar::info($image);
    }
    
    public function onAddToGallery(){
        $image = Input::all();
        $images = [];
        $gallery_images = "";
        foreach ($image['premium_gallery'] as $galleryImage) {
		 	array_push($images, (new File())->fromPost($galleryImage));
         }
        //$hover_image = (new File())->fromPost($image['hover_image']);
        foreach ($images as $gallery_image){
             $gallery_images .= '<img src="'. $gallery_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
        }
        //$hover_image = '<img src="'.$hover_image->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';
  
        return [
            '#premium_gallery' => $gallery_images
        ];
    }

    public function onSaveGallery(){
        $artisan = Artisan::where('id', Input::get('artisan_id'))->first();
        $artisan->premium_gallery = input::file('premium_gallery');
        $artisan->save();
        Flash::success('Images successfully uploaded');
        return Redirect::to('/my-account/my-gallery');

    }

	public function onGalleryImageDelete(){
        $artisan = Artisan::where('id', post('artisan_id'))->first();
        $image = post('image');
        $fileToDelete = File::where('id', $image['id'])->first();
        $fileToDelete->delete();
        Flash::success('Image successfully deleted');
        return Redirect::to('/my-account/my-gallery');
	}

    public function displayProduct(){
        $user = Auth::getUser();
        $artisan = Session::get('loginArtisan');
        $product = Product::where('slug', $this->param('slug'))->where('user_id', $artisan->id)->first();
        return $product;
	}

	public function loadCategories() {
		$categories = Category::all();
		return $categories;
	}

	public function loadChildCategories(){}
	public function onAddToCart(){
		
	}

	public function flattenArray($array) {
 $arrayValues = array();
 foreach (new RecursiveIteratorIterator( new RecursiveArrayIterator($array)) as $val) {
  $arrayValues[] = $val;
 }
 return $arrayValues;
}
    public function to_permalink($str){
		if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
			$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
		$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $str);
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
		$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
		$str = strtolower( trim($str, '-') );
		return $str;
    }

    public function getColors(){
        $this->colors = Color::all();
    }

// function flattenArrayIndexed()
	
	// public function displayManufacturerProducts(){

    // 	$this->products = Product::where('manufacturer_id', $this->manufacturer->id)->get();
    // }
    //public $manufacturers;
	public $product;
	public $products;
    public $categories;
    public $manufacturers;
    public $colors;


}