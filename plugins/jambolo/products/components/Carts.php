<?php namespace Jambolo\Products\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Jambolo\Products\Models\Manufacturer;
use Jambolo\Products\Models\Product;
use Jambolo\Products\Models\Category;
use Jambolo\Products\Models\Size;
use Jambolo\Products\Models\Price;
use Jambolo\Products\Models\Cart;
use Jambolo\Products\Models\Color;
use Jambolo\Products\Models\Frame;
use Jambolo\Products\Models\Options;
use Redirect; 
use Session;
use Auth;
use Cookie;
use Jambolo\Products\Models\Settings;

class Carts extends ComponentBase {
	public function componentDetails(){
		return [
			'name' => 'Shopping Cart',
			'description' => 'Products added to cart'
		];
	}
	public function onRun(){
		$this->page['cart'] = $this->cart();
		$this->cart = $this->cart();
		$this->dbCart = $this->dbCart();
        $this->page['cartTotal'] = Session::get('cartTotal');
		$this->total = $this->cartTotal();
		if (Settings::get('vat_value')) {
			$this->vat = Settings::get('vat_value');
		}

		$this->prepareVars();
		// $this->cartColors = $this->getColors();
		// $this->cartFrames = $this->frames();
	}

	public function prepareVars() {
		$this->shoeSizes = Options::where('shoesize','<>', '')->get();
		$this->bedSizes = Options::where('bedsize','<>', '')->get();
	}
	
	public function getColors() {
		$colors = Color::all();
		return $colors;
	}

	public function frames(){
		$frames = Frame::all();
		return $frames;
	}
    /**
     * Returns a cart, if available, 
     * 
     */

	
    public function cart()
    {
        if (!$cart = Session::has('cart') ){
            return null;
        }
		$cart = Session::get('cart');
        return $cart;
	}
	
	public function dbCart()
    {	
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		
		//dd($cookie);
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();	
			$this->dbCartTotal = $this->getDbCartTotal();	
			return $cart;
		} elseif($cookie) {
			$cart = Cart::where('cart_cookie', $cookie)->get();
			$this->dbCartTotal = $this->getDbCartTotal();	
			return $cart;
		}

		
	}
	
	public function getDbCartTotal() {
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		$cartTotal = 0;
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();
			foreach($cart as $cartItem){
				$cartTotal += $cartItem->cost;
			}
		} elseif($cookie) {
			$cart = Cart::where('cart_cookie', $cookie)->get();
			foreach($cart as $cartItem){
				$cartTotal += $cartItem->cost;
			}
		}
		return $cartTotal;
	}

    public function cartTotal()
    {
        if (!$cart = Session::has('cart') ){
            return null;
        }
        $cart = Session::get('cart');
        $total = 0;
        foreach ($cart as $cartItemKey ) {
			$total += $cartItemKey['cost'];
			//foreach ($cartItemKey as $cartItem) {
				//$total += $cartItem['cost'];
			//}
        }
        Session::put('cartTotal', $total);
        return $total;
	}

	public function findKey($array, $keySearch){
		// check if it's even an array
		if (!is_array($array)) return false;

		// key exists
		if (array_key_exists($keySearch, $array)) return true;

		// key isn't in this array, go deeper
		foreach($array as $key => $val)
		{
			// return true if it's found
			if ($this->findKey($val, $keySearch)) return true;
		}
		return false;
	}

    public function onAddToCartOld(){
		$product = Product::where('id', Input::get('product'))->first();
		$sanitizedPrice = preg_replace("/[^0-9.]/", "", $product->price);
		$quantity = Input::get('quantity');
		$cost = round($sanitizedPrice * $quantity,2);
		$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost);
		$productKey = (string) $product->slug;	
		if (!$cart = Session::has('cart') ){
			$cartArray = array();
			$cartArray[$productKey] = $cartItem;
			Session::put('cart', $cartArray);
			$cart = Session::get('cart');	
			\Flash::success('Item Successfully added to Cart');
		}
		else {
			$cartArray = Session::get('cart');
			//return $cart;
			//$cartItemUpdate = [];
			if ($this->findKey($cartArray, $product->slug)){
				$newQuantity = Input::get('quantity'); 
				$cost = round($sanitizedPrice * $newQuantity,2);
				// foreach( $cart as $cartArrayItem){
				// 	foreach($cartArrayItem as $key => $val) {
				// 		if ($this->findKey($key, $product->slug)){
				// 			unset($key[$product->slug]);
				// 		}
				// 	}
				// }
				//unset($cart[$product->slug]);
				$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost);
				//Session::forget('cart');
				Session::put('cart', $cartArray);
				//Session::push('cart', $cart);
				\Flash::success('Item quantity updated Successfully');
			}
			else {
				$cartArray = Session::get('cart');
				$cartArray[$product->slug] = $cartItem;
				//Session::put('cart',[]);
				//array_push($cart, $cartItem);
				Session::put('cart', $cartArray);
				\Flash::success('Another item Successfully added to Cart');
			}
		}
		$this->cartTotal();
		//return [
		// 	'#minicart' => $this->renderPartial('Cart::minicart')
		//];

		return \Redirect::refresh();
	}

	public function onAddToDBCart(){
		//dd(post('product_id'));
		$product = Product::where('id', post('product'))->first();
		// print_r($product['id']);
		// die;
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		if($user){	
			if (Cart::where('product_id', $product['id'])->where('item_options','LIKE','%'.json_encode(post('item_options')).'%')->where('user_id', $user['id'])->first()) {
				$checkProductInCart = Cart::where('product_id', $product['id'])->where('item_options','LIKE','%'.json_encode(post('item_options')).'%')->where('user_id', $user['id'])->first();
				if (!post('quantity')) {
					$quantity = 1;
					//dd($checkProductInCart->quantity);
					$checkProductInCart->quantity = $checkProductInCart['quantity'] + $quantity;
					$checkProductInCart->save();
					\Flash::success('Item Successfully added to Cart');
					return \Redirect::refresh();
				} else {
					$quantity = post('quantity');
					$checkProductInCart->quantity = $checkProductInCart['quantity'] + $quantity;
					$checkProductInCart->save();
					\Flash::success('Item Successfully added to Cart');
					return \Redirect::refresh();
				}
			}
		} elseif($cookie) {
			if (Cart::where('product_id', $product['id'])->where('item_options','LIKE','%'.json_encode(post('item_options')).'%')->where('cart_cookie', $cookie)->first()) {
				$checkProductInCart = Cart::where('product_id', $product['id'])->where('item_options', post('item_options'))->where('cart_cookie', $cookie)->first();
				if (!post('quantity')) {
					$quantity = 1;
					//dd($checkProductInCart->quantity);
					$checkProductInCart->quantity = $checkProductInCart['quantity'] + $quantity;
					$checkProductInCart->cost = $checkProductInCart['quantity']['price'] * $checkProductInCart['quantity'];
					$checkProductInCart->save();
					\Flash::success('Item Successfully added to Cart');
					return \Redirect::refresh();
				} else {
					$quantity = post('quantity');
					$checkProductInCart->quantity = $checkProductInCart['quantity'] + $quantity;
					$checkProductInCart->cost = $checkProductInCart['product']['price'] * $checkProductInCart['quantity'];
					$checkProductInCart->save();
					\Flash::success('Item Successfully added to Cart');
					return \Redirect::refresh();
				}
			}
		} 
		if (!Input::get('quantity')) {
			$quantity = 1;
		} else {
			$quantity = Input::get('quantity');
		}
		$sanitizedPrice = post('price');
		// $size = Input::get('size');
		$cost = round($sanitizedPrice * $quantity,2);
		$newCartItem = new Cart;
		$newCartItem->item_options = json_encode(post('item_options'));
		$newCartItem->product_id = $product['id'];
		// $newCartItem->price = $sanitizedPrice;
		$newCartItem->cost = $cost;
		//$newCartItem->size = $size;
		$user = Auth::getUser();
		$cookie = Cookie::get('cartCookie');
		if($user){	
			$newCartItem->user_id = $user['id'];
		} elseif($cookie) {
			Cookie::queue('cartCookie',$cookie, 360);
			//setcookie("cartCookie", $cookie, time() + 3600 * 24 * 7);
			$newCartItem->cart_cookie = $cookie;
		} 
		else {
			$uniqueVal = uniqid();
			Cookie::queue('cartCookie', $uniqueVal, 360);
			//$cookie = Cookie::forever('cartCookie', $uniqueVal);
			//Cookie::make('cartCookie',$uniqueVal,60*24*7);
			//setcookie("cartCookie", $uniqueVal, time() + 3600 * 24 * 7);
			$newCartItem->cart_cookie = $uniqueVal;
		}
		$newCartItem->quantity = $quantity;
		//dd($newCartItem);
		$newCartItem->save();
		\Flash::success('Item Successfully added to Cart');
		return \Redirect::refresh();
	}

	public function onAddToCart(){
		$product = Product::where('id', Input::get('product'))->first();
		//$sanitizedPrice = preg_replace("/[^0-9.]/", "", $product->price);
		$quantity = Input::get('quantity');
		if (Input::get('size')) {
			$size = Input::get('size');
		}
		$sanitizedPrice = Input::get('price');
		//$sanitizedPrice = $sanitizedPrice->price;
		$color = Input::get('color');
		$frame = Input::get('frame');
		$cost = round($sanitizedPrice * $quantity,2);
		//return $frame.$color.$size;
		//dd();
		$this->productSizes = [
			'5 x 7' => $product->base_price->five_by_seven,
			'8 x 12' => $product->base_price->eight_by_twelve,
			'12 x 16' => $product->base_price->twelve_by_sixteen,
			'16 x 20' => $product->base_price->sixteen_by_twenty,
			'20 x 20' => $product->base_price->twenty_by_twenty,
			'20 x 38' => $product->base_price->twenty_by_twenty_eight,
			'24 x 36' => $product->base_price->twenty_four_by_thirty_six,
			'28 x 39' => $product->base_price->twenty_eight_by_thirty_nine
		];

		$cost = round($sanitizedPrice * $quantity,2);
		if ($product->homeitem) {
			$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost);
			
		} else {
			$cartItem = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$quantity,'cost'=>$cost, 'size'=>$size, 'frame'=>$frame);
			//return "did you get here?";
		}
		if ($product->homeitem) {
			$productKey = (string) $product->slug;	
		} else {
			$productKey = (string) $product->slug.'/'.$size.'/'.$frame;	
		}
		if (!$cart = Session::has('cart') ){
			$cartArray = array();
			$cartArray[$productKey] = $cartItem;
			Session::put('cart', $cartArray);
			$cart = Session::get('cart');	
			\Flash::success('Item Successfully added to Cart');
		}
		else {
			$cartArray = Session::get('cart');
			if ($this->findKey($cartArray, $product->slug)){
				$newQuantity = Input::get('quantity'); 
				$cost = round($sanitizedPrice * $newQuantity,2);
				if ($product->homeitem) {
					$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost);
				} else {
					$cartArray[$productKey] = array('id'=>$product->id,'product_image'=>$product->product_image->path,'slug'=>$product->slug,'price'=>$sanitizedPrice,'product'=>str_replace("<br />","",$product->product_name),'quantity'=>$newQuantity,'cost'=>$cost, 'size'=>$size, 'frame'=>$frame);
				}
				//Session::forget('cart');
				Session::put('cart', $cartArray);
				//Session::push('cart', $cart);
				\Flash::success('Item quantity updated Successfully');
			}
			else {
				$cartArray = Session::get('cart');
				$cartArray[$product->slug] = $cartItem;
				//Session::put('cart',[]);
				//array_push($cart, $cartItem);
				Session::put('cart', $cartArray);
				\Flash::success('Another item Successfully added to Cart');
			}
		}
		$this->cartTotal();
		//return [
		// 	'#minicart' => $this->renderPartial('Cart::minicart')
		//];
		
		return \Redirect::to('/prints');
	}

	public function onRemoveFromDbCart(){
		$cartItemId = post('cartitemid');
		$cartItem = Cart::where('id', $cartItemId)->first();
		$cartItem->delete();
		$this->dbCartTotal = $this->getDbCartTotal();
		\Flash::success('Item was successfully removed from the Cart');
		return \Redirect::refresh();

	}

	public function onEmptyDbCart(){
		$user = Auth::getUser();
		if($user){
			$cart = Cart::where('user_id', $user->id)->get();
		} else {
			$cart = Cart::where('cart_cookie', Cookie::get('cartCookie'))->get();
		}
		foreach ($cart as $cartItem) {
			$cartItem->delete();
		}
		return \Redirect::refresh();
	}

	// public function onDbUpdateItem(){
	// 	$cartItemId = post('cartitemid');
	// 	$cartItem = Cart::where('id', $cartItemId)->first();
	// 	$cartItem->quantity = $quantity[0] = $quantity = post('quantity');
	// 	$cartItem->size = $size[0] = $size = post('size');
	// 	$cartItem->frame = $frame[0] = $frame = post('frame');
	// 	$cartItem->total = $total[0] = $total = post('cartitemtotalcost');
	// 	$cartItem->save();

	// 	$this->dbCartTotal = $this->getDbCartTotal();
	// 	\Flash::success('Cart updated successfully');
	// 	return Redirect::to('/cart');
	// }

	public function onDBCartUpdate(){
		$cartUpdateInput = Input::all();

		//dd($cartUpdateInput);
		$sizes = post('size');
		//$frames = post('frame');
		$products = post('product');
		$quantities = post('quantity');
		$costs = post('cartitemtotalcost');
		$cartItemIds = post('cartitemid');
		$cartCount = post('cartcount');
		////// find current user cart
		// find logged in user
		$user = Auth::getUser();
		
		for ($i=0; $i <= $cartCount - 1; $i++) { 
			$cart = Cart::where('id', $cartItemIds[$i] )->first();
			$size = $sizes[$i];
			if (!$size=="NA") {
				//$cart->size = $sizes[$i];
				//$sizeInText = $this->sizeMap[$size];
				$product = Product::where('id', $products[$i])->first();
				//$cart->price = $product->base_price[$sizeInText];
				//$cart->frame = $frames[$i];
			} else {
				$cart->size = $sizes[$i];
				//dd($sizes[$i]);
				$product = Product::where('id', $products[$i])->first();
				//$cart->price = $product->price;
			}
			$cart->quantity = $quantities[$i];
			//dd($quantities[$i]);
			$cart->cost = $product->price * $quantities[$i];
			$cart->save();
		}
		
		$this->dbCartTotal = $this->getDbCartTotal();
		\Flash::success('Cart updated successfully');
		return Redirect::to('/marketplace/cart');
	}



	public function tofloat($num) {
		$dotPos = strrpos($num, '.');
		$commaPos = strrpos($num, ',');
		$sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
			((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

		if (!$sep) {
			return floatval(preg_replace("/[^0-9]/", "", $num));
		} 

		return floatval(
			preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
			preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
		);
	}

	public function onFlush(){
		Session::forget('cart');
		//Session::forget('cart');
		Session::forget('carttotal');
		\Flash::success('Cart cleared successfully');
		 return Redirect::to('/prints');
	}

	public function onSizeSelectCart(){
		$sizeInText = post('sizeInText');
		$product = Product::where('id',post('product'))->first();
		$price = $product->base_price[$sizeInText];
		$index = post('index');
		$quantity = post('quantity');
		$cost = $price * $quantity;
		return [
			".finalprice$index" => $this->renderPartial('@finalprice', ['price' => $price]),
			".itemtotalcost$index" => $this->renderPartial('@itemtotalcost', ['cost' => $cost]),
			".quantity$index" => $this->renderPartial('@quantityreset')
		];
	}

	public function onChangeQuantityCart(){
		$index = post('index');
		$cost = post('cost');
		return [
			".itemtotalcost$index" => $this->renderPartial('@itemtotalcost', ['cost' => $cost])
		];
	}

	public function onOptions(){
		$data = post();
		//$item_options = $data['item_options'];
		//print_r($item_options['bedsize']);
		//die;
		$product = Product::where('id', $data['product'])->first();
		$size = Options::where('bedsize', $data['item_options']['bedsize'])->first();
		$bedfloor = Options::where('bedfloor', $data['item_options']['bedfloor'])->first();
		$drawers = Options::where('drawer', $data['item_options']['drawers'])->where('bedsize','')->where('shoesize','')->where('bedfloor','')->first();
		//$quantity = $data['quantity'];
		$totalCost = ($product->price + $size->cost + $bedfloor->cost + $drawers->cost); 
		return [
			"#product-price" => $this->renderPartial('@productprice', ['totalCost' => $totalCost])
		];
		//print_r($data);
		//print_r($totalCost);
		//die;
	}

	public $shoeSizes;
	public $bedSizes;
	

	public $cart;
	public $total;
	public $dbCartTotal;
	public $productSizes;
	public $cartColors;
	public $cookie;
	public $cartFrames;
	public $sizeMap = [
		'5 x 7' => 'five_by_seven',
		'8 x 12' => 'eight_by_twelve',
		'12 x 16' => 'twelve_by_sixteen',
		'16 x 20' => 'sixteen_by_twenty',
		'20 x 20' => 'twenty_by_twenty',
		'20 x 38' => 'twenty_by_twenty_eight',
		'24 x 36' => 'twenty_four_by_thirty_six',
		'28 x 39' => 'twenty_eight_by_thirty_nine'
	];
	public $sizeMap2 = [
		'5+x+7' => 'five_by_seven',
		'8+x+12' => 'eight_by_twelve',
		'12+x+16' => 'twelve_by_sixteen',
		'16+x+20' => 'sixteen_by_twenty',
		'20+x+20' => 'twenty_by_twenty',
		'20+x+38' => 'twenty_by_twenty_eight',
		'24+x+36' => 'twenty_four_by_thirty_six',
		'28+x+39' => 'twenty_eight_by_thirty_nine'
	];
	public $productSizesMap = [
        '12cmx18cm-5x7' => "five_by_seven",
        '21cmx30cm-8x12' => "eight_by_twelve",
        '30cmx40cm-12x16' => "twelve_by_sixteen",
        '40cmx50cm-16x20' => "sixteen_by_twenty",
        '50cmx50cm-20x20' => "twenty_by_twenty",
        '50cmx70cm-20x28' => "twenty_by_twenty_eight",
        '61cmx91cm-24x36' => "twenty_four_by_thirty_six",
        '70cmx100cm-28x39' => "twenty_eight_by_thirty_nine"
	];
	

	public $vat;
}