<?php namespace Jambolo\Products\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Jambolo\Products\Models\Order;
use Jambolo\Products\Models\OrderDetails;
use Redirect; 
use Auth;
use Jambolo\Products\Models\Settings;
use Jambolo\Products\Models\Product;
use Jambolo\Products\Models\Bedsize;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Products\Models\Options;


class SingleProduct extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Single Product',
			'description' => 'Single Product Details'
		];
	}



	public function onRun(){
        $this->prepareVars();
        $this->displaySingleProduct();


    }

    // public function displayManufacturer(){
    // 	$this->manufacturer = Manufacturer::where('slug', $this->param('slug'))->first();
    // }
    
    public function displaySingleProduct(){


        //$this['artisan'] = Artisan::where('slug',$this->param('slug'))->first();
        $this->product = Product::where('slug', $this->param('product-slug'))->first();
        $product = Product::where('slug', $this->param('product-slug'))->first();
        //dd($product);
        $this->bedSizes = $product->bedsizes;
        //dd($this->product->category->slug);
        // echo $bedSizes;
        // echo '<br/>';
        // echo $this->bedSizes;
        // dd();
        //dd($product->bedsizes);
		$productCategory = $this->product->category->slug;
		$sizes = "";
		if($productCategory == 'bed'){
			$sizes = $this->product->bedsizes;
		}
		elseif($productCategory == 'shoes'){
			$sizes = $this->product->shoesizes;
		}
        $this->sizes = $sizes;	
        //$this->availBedSizes = array_intersect($bedSizes, $this->bedSizes);
    
    }

    public function prepareVars() {
        $this->shoeSizes = Options::where('shoesize','<>', '')->get();
		$this->bedSizes = Options::where('bedsize','<>', '')->get();
	}
	

	public $sizes;
    public $product;
    public $shoeSizes;
    public $bedSizes;
    public $availBedSizes;
	
	//public $products;

}