<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class ProductAttribute extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_attributes';

    public $belongsTo = [
        'product' => ['Jambolo\Products\Models\Product', 'key' =>'product_id', 'other_key' => 'id'],
    ];

    public $belongsToMany = [
        'attributevalues' => [
            'Jambolo\Attributes\Models\AttributeValue',
            'table' => 'jambolo_products_attribute_value_product_attribute_value',
        ]
        
        
    ];
}