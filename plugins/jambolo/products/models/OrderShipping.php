<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class OrderShipping extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_order_shipping';

    public $belongsTo = [
        'order' => 'Jambolo\Products\Models\Order'
    ];

    public $hasMany = [
        'orders' => 'Jambolo\Products\Models\Order',
    ];
}