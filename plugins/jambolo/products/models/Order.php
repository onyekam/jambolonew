<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_order_summary';

    public $hasOne = [
        'delivery_term' => ['Jambolo\Products\Models\DeliveryTerms', 'key' => 'id', 'otherKey' => 'delivery_terms_id'],
        
        'ordershipping'=> 'Jambolo\Products\Models\OrderShipping'
    ];

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',
        'status' => 'Jambolo\Products\Models\Status',
        'payment_method' => 'Jambolo\Products\Models\PaymentMethod', 
        
    ];

    public $belongsToMany = [
        
        
    ];

    public $hasMany = [
        'order_details' => 'Jambolo\Products\Models\OrderDetails',
    ];


    // public $belongsTo = [
        
    // ];
}