<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class OrderDetails extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_order_details';

    public $hasOne = [
        'order' => [ 'Jambolo\Products\Models\Order', 'key'=>'id', 'otherKey'=>'order_id' ],
        'product' => ['Jambolo\Products\Models\Product','key'=>'id','otherKey'=>'product_id'],
        'size' => ['Jambolo\Products\Models\Size','key'=>'id','otherKey'=>'size_id'],
        'color' => ['Jambolo\Products\Models\Color','key'=>'id','otherKey'=>'color_id'],
        'frame' => ['Jambolo\Products\Models\Frame','key'=>'id','otherKey'=>'frame_id']
        
    ];

    public $belongsTo = [
        'order' => [ 'Jambolo\Products\Models\Order', 'key'=>'id', 'otherKey'=>'order_id' ],
        'artisan' => 'Jambolo\Artisans\Models\Artisan'
       
    ];


}