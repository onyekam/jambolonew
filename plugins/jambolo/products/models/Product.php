<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_products';

    public $attachOne = [
        'product_image' => 'System\Models\File'
    ];

    public $attachMany = [
        'other_images' => 'System\Models\File'
    ];

    public $belongsToMany = [
        'shoesizes' => [
            'Jambolo\Products\Models\Shoesize',
            'table' => 'jambolo_products_products_shoesizes',
            'otherKey'      => 'shoesize_id',
            'key' => 'product_id'
        ],
        'bedsizes' => [
            'Jambolo\Products\Models\Bedsize',
            'table' => 'jambolo_products_bedsizes_products',
            'otherKey'      => 'bedsize_id',
            'key' => 'product_id'
        ],
    ];

    public $belongsTo = [
        'artisan' => ['Jambolo\Artisans\Models\Artisan'],
        'category' => ['Jambolo\Products\Models\ProductCategory'],
        'user' => 'Rainlab\User\Models\User',
        'carts' => 'Jambolo\Products\Models\Cart',
        
    ];

    public $hasOne = [
        'feature' => ['Jambolo\Ads\Models\ProductFeatures'],
        //'category' => ['Jambolo\Products\Models\ProductCategory'],
        'attribute' => ['Jambolo\Attributes\Models\Attribute'],
    ];

    
}