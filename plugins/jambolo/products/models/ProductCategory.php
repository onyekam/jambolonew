<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class ProductCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_categories';

    public $hasMany = [
        'categories' => 'Jambolo\Products\Models\ProductCategory',
        'products' => 'Jambolo\Products\Models\Product'
    ];

    public $hasOne = [
        'parent_category' => [
            'Jambolo\Products\Models\ProductCategory',
            'key' => 'id',
            'otherKey' => 'category_id'
        ]
    ];

    public $belongsToMany = [
        'child_categories' => [
            'Jambolo\Products\Models\ProductCategory',
            'table' => 'jambolo_products_categories_categories',
            'order' => 'category_name',
            'key' => 'category_id',
            'otherKey' => 'child_category_id'
        ]
    ];

    public function childrenAccounts(){
        return $this->hasMany(ProductCategory::Class, 'category_id');
     }

    public function allChildrenAccounts(){
        return $this->hasMany(ProductCategory::Class, 'category_id')->with('childrenAccounts');
    }
}