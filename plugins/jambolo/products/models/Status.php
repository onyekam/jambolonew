<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class Status extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_status';

    // public $belongsToMany = [
    //     'orders' => 'Ffande\Procurement\Models\Order',
    // ];

    public $hasMany = [
        'orders' => 'Jambolo\Products\Models\Order',
    ];
}