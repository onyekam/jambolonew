<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class Shoesize extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_shoesizes';

    public $belongsToMany = [
        'products' => [
            'Jambolo\Products\Models\Product',
            'table' => 'jambolo_products_products_shoesizes',
            'key'      => 'shoesize_id',
            'otherKey' => 'product_id'
        ]
    ];

}