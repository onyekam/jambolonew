<?php namespace Jambolo\Products\Models;

use Model;

/**
 * Model
 */
class Cart extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_products_cart';

    public $belongsTo = [
        'user' => ['Rainlab\User\Models\User'],
        'product' => ['Jambolo\Products\Models\Product']
    ];
}