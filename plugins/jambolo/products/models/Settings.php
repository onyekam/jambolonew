<?php namespace Jambolo\Products\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'jambolo_products_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}

