<?php namespace Jambolo\Products;

use System\Classes\PluginBase;
use Jambolo\Products\Models\Settings;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
    	
            'Jambolo\Products\Components\DashboardProducts' => 'DashboardProducts',
            'Jambolo\Products\Components\Products' => 'products',
            'Jambolo\Products\Components\Carts' => 'Cart',
            'Jambolo\Products\Components\Shop' => 'Shop',
            'Jambolo\Products\Components\Checkout' => 'Checkout',
            'Jambolo\Products\Components\Orders' => 'orders',
            'Jambolo\Products\Components\DisplayOrderDetails' => 'displayorderdetails',
            'Jambolo\Products\Components\SingleProduct' => 'singleproduct',

    	];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'VAT Settings',
                'icon'        => 'icon-money',
                'description' => 'Manage VAT Settings',
                'class'       => 'Jambolo\Products\Models\Settings',
                'order'       => 10
                // 'permissions' => ['toughdeveloper.imageresizer.access_settings']
            ]
        ];
    }

    public function boot() {

    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                // A local method, i.e $this->makeTextAllCaps()
                'json_decode' => [$this, 'json_decode'],
                'cast_to_array' => [$this, 'cast_to_array']
            ]
        ];
    }

    public function json_decode($text)
    {
        return json_decode($text);
    }

    public function cast_to_array($text)
    {
        return (array)$text;
    }
}
