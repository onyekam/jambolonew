<?php namespace Jambolo\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddNewFields extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('phone_number')->nullable();
            

        });
    }

    public function down()
    {
        Schema::dropDown([
                'phone_number'
            ]);
    }

}
