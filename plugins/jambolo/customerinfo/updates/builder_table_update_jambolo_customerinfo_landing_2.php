<?php namespace Jambolo\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloCustomerinfoLanding2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_customerinfo_landing', function($table)
        {
            $table->integer('area_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_customerinfo_landing', function($table)
        {
            $table->dropColumn('area_id');
        });
    }
}
