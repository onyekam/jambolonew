<?php namespace Jambolo\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloCustomerinfoLanding extends Migration
{
    public function up()
    {
        Schema::table('jambolo_customerinfo_landing', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->renameColumn('artisan_id', 'artisan_category_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_customerinfo_landing', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->renameColumn('artisan_category_id', 'artisan_id');
        });
    }
}
