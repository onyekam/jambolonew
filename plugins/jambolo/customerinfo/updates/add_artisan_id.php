<?php namespace Jambolo\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddArtisanId extends Migration
{

    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->string('artisan_id')->nullable();
            

        });
    }

    public function down()
    {
        Schema::dropDown([
                'artisan_id'
            ]);
    }

}
