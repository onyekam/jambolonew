<?php namespace Jambolo\Customerinfo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloCustomerinfoLanding3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_customerinfo_landing', function($table)
        {
            $table->integer('artisan_category_id')->nullable()->change();
            $table->integer('area_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_customerinfo_landing', function($table)
        {
            $table->integer('artisan_category_id')->nullable(false)->change();
            $table->integer('area_id')->nullable(false)->change();
        });
    }
}
