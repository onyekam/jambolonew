<?php namespace Jambolo\Customerinfo;

use System\Classes\PluginBase;
use Rainlab\User\Controllers\Users as UsersController;
use Rainlab\User\Models\User as UserModel;
use Jambolo\Artisans\Models\Artisan as ArtisanModel;
use Jambolo\Customerinfo\Models\Landing;
use Flash;
use Redirect;
use Response;
use RainLab\User\Models\Settings as UserSettings;
use Auth;
use Mail;
use Event;
use Jambolo\Products\Models\Cart;
use Cookie;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot() {

        UserModel::extend(function($model){
            $model->addFillable([
                'phone_number',
                'artisan_id'
            ]);
            $model->hasOne['listing'] = ['Jambolo\Artisans\Models\Artisan', 'key' => 'id', 'otherKey' => 'artisan_id'];

            
        });

        UserModel::extend(function($model){
            $model->bindEvent('model.afterCreate', function() use ($model) {
                if( post('area_id') ){
                    $landing = new Landing;
                    //$model->landing->user_id = $model->id;
                    $landing->user_id = $model->id;
                    //$landing->artisan_category_id = post('category_id');
                    $landing->area_id = post('area_id');
                    $landing->save();
                    Flash::success("Registration Successful");
                    $file = storage_path('app/media/The_Trusted_Nigerian_Artisans_Directory.pdf');
                    //Redirect::to(storage_path('app/media/The_Trusted_Nigerian_Artisans_Directory.pdf'));
                    //return Response::download($file);
                    \Log::info('just saved model $landing->id ');
                }
            });
        });

        ArtisanModel::extend(function($model){
            $model->bindEvent('model.afterUpdate', function() use ($model) {
                if($model->premium == 1){
                    $createData = [
                        'name' => $model->company_name,
                        'artisan_id' => $model->id,
                        'password' => '0000',
                        'password_confirmation' => '0000',
                        'email' => $model->email,
                        'username' => $model->phone_number,
                        'phone_number' => $model->phone_number,
                    ];

                    Event::fire('rainlab.user.beforeRegister', [&$createData]);

                    $requireActivation = UserSettings::get('require_activation', true);
                    $automaticActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_AUTO;
                    $userActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_USER;
                    $user = Auth::register($createData, $automaticActivation);

                    Event::fire('rainlab.user.register', [$user, $createData]);
                    //$premiumArtisan = UserModel::firstOrCreate($createData);
                }
            });
        });

        Event::listen('rainlab.user.login', function($user) {
            //dd($user);
            // if ($user->newsletter == 1) {
            //     $newsletter = NewsletterList::where('email',$user->email)->first();
            //     if (!$newsletter) {
            //         $newNewsletterUser = New NewsletterList;
            //         $newNewsletterUser->email = $user->email;
            //         $newNewsletterUser->save();
            //     }
            //     $userModel = UserModel::where('id', $user->id)->first();
            // }
            $currentCookie = Cookie::get('cartCookie');
            $cartItemWithCookie = Cart::where('cart_cookie', $currentCookie)->get();
            foreach ($cartItemWithCookie as $cartItem) {
                $cartItem->user_id = $user->id;
                $cartItem->save();
            }
            Cookie::queue(\Cookie::forget('cartCookie'));
            \Log::info('Cart Updated');
        });

        // Event::listen('rainlab.user.beforeAuthenticate', function($user) {
        //     $user = UserModel::where('login', $post('login'))->first();
        //     if(!($user->premium)){

        //     }
        // });

     
        //$model->belongsTo['type'] = ['Ffande\Customerinfo\Models\Profession', 'key' =>'id', 'otherKey' => 'type_id'];
        //$model->belongsTo['landing'] = ['Jambolo\Customerinfo\Models\Landing'];
        //$model->hasMany['products'] = ['Jambolo\Products\Models\Product', 'key' => 'id', 'otherKey' => 'artisan_id'];
        
        
        
    	UsersController::extendFormFields(function($form, $model, $context){

    		$form->addTabFields([
    			'phone_number' => [
    				'label' => 'Phone Number',
    				'type' => 'text',
    				'tab' => 'Profile'
                ],
                'artisan_id' => [
    				'label' => 'Artisan ID',
    				'type' => 'text',
    				'tab' => 'Profile'
                ]
    			]);

    	});

    }
}
