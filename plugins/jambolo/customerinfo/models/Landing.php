<?php namespace Jambolo\Customerinfo\Models;

use Model;

/**
 * Model
 */
class Landing extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_customerinfo_landing';

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',
        'category' => ['Jambolo\Artisans\Models\ArtisanCategory', 'key' => 'artisan_category_id', 'other_key' => 'id'],
        'area' => 'Jambolo\Artisans\Models\Area', 
    ];
}