<?php namespace Jambolo\Artisans\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Area extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'jambolo.artisans.manage_area' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Jambolo.Artisans', 'Artisans', 'Areas');
    }
}