<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisans3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
            $table->string('fname', 30)->nullable()->change();
            $table->string('lname', 30)->nullable()->change();
            $table->smallInteger('categoryid')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->dropColumn('deleted_at');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
            $table->string('fname', 30)->nullable(false)->change();
            $table->string('lname', 30)->nullable(false)->change();
            $table->smallInteger('categoryid')->nullable(false)->change();
        });
    }
}
