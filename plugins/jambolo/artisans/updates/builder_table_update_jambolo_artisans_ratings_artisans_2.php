<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansRatingsArtisans2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->integer('rating_quality')->nullable();
            $table->integer('rating_pricing')->nullable();
            $table->renameColumn('rating', 'rating_service');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->dropColumn('rating_quality');
            $table->dropColumn('rating_pricing');
            $table->renameColumn('rating_service', 'rating');
        });
    }
}
