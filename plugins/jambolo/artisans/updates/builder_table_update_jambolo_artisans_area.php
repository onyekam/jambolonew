<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArea extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->integer('city_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->dropColumn('city_id');
        });
    }
}
