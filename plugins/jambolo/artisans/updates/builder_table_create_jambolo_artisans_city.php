<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansCity extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_city', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('city_id')->unsigned();
            $table->string('city_name', 25);
            $table->string('slug', 25);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_city');
    }
}
