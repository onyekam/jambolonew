<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansRatingsArtisans3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->integer('user_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}
