<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansCategory4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->integer('parent_category_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->dropColumn('parent_category_id');
        });
    }
}
