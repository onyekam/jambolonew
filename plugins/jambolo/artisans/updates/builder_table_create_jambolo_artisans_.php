<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisans extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('artisanid')->unsigned();
            $table->string('fname', 30);
            $table->string('lname', 30);
            $table->string('address', 255);
            $table->smallInteger('stateid');
            $table->smallInteger('cityid');
            $table->smallInteger('areaid');
            $table->decimal('latitude', 8, 6);
            $table->decimal('longitude', 9, 6);
            $table->smallInteger('premium')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_');
    }
}
