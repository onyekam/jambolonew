<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansAssociation extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_association', function($table)
        {
            $table->text('about_us')->nullable();
            $table->string('abbreviation', 12)->nullable();
            $table->text('services')->nullable();
            $table->text('partnership')->nullable();
            $table->text('leadership')->nullable();
            $table->dropColumn('logo');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_association', function($table)
        {
            $table->dropColumn('about_us');
            $table->dropColumn('abbreviation');
            $table->dropColumn('services');
            $table->dropColumn('partnership');
            $table->dropColumn('leadership');
            $table->string('logo', 25)->nullable();
        });
    }
}
