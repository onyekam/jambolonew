<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArtisan7 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->decimal('overall_average_rating', 10, 3)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->decimal('overall_average_rating', 5, 5)->change();
        });
    }
}
