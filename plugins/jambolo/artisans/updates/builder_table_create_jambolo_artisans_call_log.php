<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansCallLog extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_call_log', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('artisan_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_call_log');
    }
}
