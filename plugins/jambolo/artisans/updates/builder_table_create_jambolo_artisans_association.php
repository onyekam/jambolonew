<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansAssociation extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_association', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('association_name', 25);
            $table->string('slug', 25)->nullable();
            $table->string('logo', 25)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_association');
    }
}
