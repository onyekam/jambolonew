<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansState extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_state', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('state_id')->unsigned();
            $table->string('state_name', 25);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_state');
    }
}
