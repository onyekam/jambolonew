<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansAssociationExecutives extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_association_executives', function($table)
        {
            $table->string('slug', 50)->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_association_executives', function($table)
        {
            $table->dropColumn('slug');
            $table->increments('id')->unsigned()->change();
        });
    }
}
