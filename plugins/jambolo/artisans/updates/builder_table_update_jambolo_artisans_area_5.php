<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArea5 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->string('priority2', 10)->nullable()->unsigned(false)->default(null)->change();
            $table->string('priority3', 10)->nullable()->unsigned(false)->default(null)->change();
            $table->string('priority4', 10)->nullable()->unsigned(false)->default(null)->change();
            $table->string('priority5', 10)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->integer('priority2')->nullable()->unsigned(false)->default(null)->change();
            $table->integer('priority3')->nullable()->unsigned(false)->default(null)->change();
            $table->integer('priority4')->nullable()->unsigned(false)->default(null)->change();
            $table->integer('priority5')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
