<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansAssociation2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_association', function($table)
        {
            $table->string('association_name', 150)->change();
            $table->string('slug', 150)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_association', function($table)
        {
            $table->string('association_name', 25)->change();
            $table->string('slug', 25)->change();
        });
    }
}
