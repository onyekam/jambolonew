<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansCategory3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->renameColumn('categoryid', 'id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->renameColumn('id', 'categoryid');
        });
    }
}
