<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansParentCategories extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_parent_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('category_name');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_parent_categories');
    }
}
