<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisans extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->smallInteger('state_id')->nullable();
            $table->smallInteger('city_id')->nullable();
            $table->smallInteger('area_id')->nullable();
            $table->smallInteger('listing_category_id');
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
            $table->dropColumn('stateid');
            $table->dropColumn('cityid');
            $table->dropColumn('areaid');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->dropColumn('state_id');
            $table->dropColumn('city_id');
            $table->dropColumn('area_id');
            $table->dropColumn('listing_category_id');
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
            $table->smallInteger('stateid');
            $table->smallInteger('cityid');
            $table->smallInteger('areaid');
        });
    }
}
