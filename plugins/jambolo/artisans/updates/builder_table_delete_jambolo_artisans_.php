<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteJamboloArtisans extends Migration
{
    public function up()
    {
        Schema::dropIfExists('jambolo_artisans_');
    }
    
    public function down()
    {
        Schema::create('jambolo_artisans_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('artisanid')->unsigned();
            $table->string('fname', 30)->nullable();
            $table->string('lname', 30)->nullable();
            $table->string('address', 255)->nullable();
            $table->decimal('latitude', 8, 6)->nullable();
            $table->decimal('longitude', 9, 6)->nullable();
            $table->smallInteger('premium')->nullable()->default(0);
            $table->smallInteger('state_id')->nullable();
            $table->smallInteger('city_id')->nullable();
            $table->smallInteger('area_id')->nullable();
            $table->smallInteger('categoryid')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('phone_number', 12)->nullable();
        });
    }
}
