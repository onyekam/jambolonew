<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansState extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_state', function($table)
        {
            $table->renameColumn('state_id', 'id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_state', function($table)
        {
            $table->renameColumn('id', 'state_id');
        });
    }
}
