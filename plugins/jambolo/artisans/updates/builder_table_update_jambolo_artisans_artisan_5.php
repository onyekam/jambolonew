<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArtisan5 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->decimal('latitude', 10, 7)->change();
            $table->decimal('longitude', 10, 7)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->decimal('latitude', 8, 6)->change();
            $table->decimal('longitude', 8, 6)->change();
        });
    }
}
