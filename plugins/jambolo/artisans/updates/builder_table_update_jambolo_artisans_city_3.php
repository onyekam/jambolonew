<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansCity3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_city', function($table)
        {
            $table->renameColumn('city_id', 'id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_city', function($table)
        {
            $table->renameColumn('id', 'city_id');
        });
    }
}
