<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansCategory2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->text('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
