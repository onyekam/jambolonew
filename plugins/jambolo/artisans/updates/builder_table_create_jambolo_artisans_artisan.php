<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansArtisan extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_artisan', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('artisanid');
            $table->string('first_name', 15)->nullable();
            $table->string('last_name', 15)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('phone_number', 16)->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('area_id')->nullable();
            $table->integer('premium')->nullable()->default(0);
            $table->integer('category_id')->nullable();
            $table->decimal('latitude', 8, 6)->nullable();
            $table->decimal('longitude', 9, 6)->nullable();
            $table->integer('association_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_artisan');
    }
}
