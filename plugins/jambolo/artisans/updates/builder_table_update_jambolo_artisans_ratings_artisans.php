<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansRatingsArtisans extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->text('comments')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->dropColumn('comments');
        });
    }
}
