<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArea6 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->integer('zone_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->dropColumn('zone_id');
        });
    }
}
