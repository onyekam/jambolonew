<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansAssociationExecutives extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_association_executives', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->string('position', 50)->nullable();
            $table->string('phone_number', 11)->nullable();
            $table->integer('association_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_association_executives');
    }
}
