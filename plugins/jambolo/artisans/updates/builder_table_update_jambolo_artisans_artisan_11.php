<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArtisan11 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->string('first_name', 100)->change();
            $table->string('company_name', 100)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->string('first_name', 15)->change();
            $table->string('company_name', 50)->change();
        });
    }
}
