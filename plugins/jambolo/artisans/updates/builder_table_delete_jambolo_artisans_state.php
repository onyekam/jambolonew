<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteJamboloArtisansState extends Migration
{
    public function up()
    {
        Schema::dropIfExists('jambolo_artisans_state');
    }
    
    public function down()
    {
        Schema::create('jambolo_artisans_state', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('state_id')->unsigned();
            $table->string('state_name', 25);
            $table->string('slug', 25);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
}
