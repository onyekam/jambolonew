<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArtisan3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->string('slug', 100)->nullable();
            $table->string('first_name', 15)->nullable(false)->change();
            $table->string('last_name', 15)->nullable(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->dropColumn('slug');
            $table->string('first_name', 15)->nullable()->change();
            $table->string('last_name', 15)->nullable()->change();
        });
    }
}
