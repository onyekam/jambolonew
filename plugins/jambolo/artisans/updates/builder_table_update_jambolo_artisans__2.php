<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisans2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
            $table->renameColumn('listing_category_id', 'categoryid');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
            $table->renameColumn('categoryid', 'listing_category_id');
        });
    }
}
