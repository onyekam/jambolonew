<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansAssociationZonalChairmen extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_association_zonal_chairmen', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->integer('area_id')->nullable();
            $table->string('phone_number', 11)->nullable();
            $table->string('slug', 50)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_association_zonal_chairmen');
    }
}
