<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArtisan13 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->integer('call_counter')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->dropColumn('call_counter');
        });
    }
}
