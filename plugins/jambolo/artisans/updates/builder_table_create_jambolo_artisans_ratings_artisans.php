<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansRatingsArtisans extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_ratings_artisans', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('artisan_id')->nullable();
            $table->integer('rating')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_ratings_artisans');
    }
}
