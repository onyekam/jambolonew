<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansCategory5 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->text('seotitle')->nullable();
            $table->text('seodescription')->nullable();
            $table->text('keywords')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_category_', function($table)
        {
            $table->dropColumn('seotitle');
            $table->dropColumn('seodescription');
            $table->dropColumn('keywords');
        });
    }
}
