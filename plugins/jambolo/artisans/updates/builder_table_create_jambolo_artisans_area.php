<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansArea extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_area', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('area_id')->unsigned();
            $table->string('area_name', 25);
            $table->string('slug', 25);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_area');
    }
}
