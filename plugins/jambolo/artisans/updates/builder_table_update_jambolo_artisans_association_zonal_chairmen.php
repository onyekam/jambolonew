<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansAssociationZonalChairmen extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_association_zonal_chairmen', function($table)
        {
            $table->integer('association_id')->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_association_zonal_chairmen', function($table)
        {
            $table->dropColumn('association_id');
            $table->increments('id')->unsigned()->change();
        });
    }
}
