<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisans4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->string('phone_number', 12)->nullable();
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_', function($table)
        {
            $table->dropColumn('phone_number');
            $table->increments('artisanid')->nullable(false)->unsigned()->default(null)->change();
        });
    }
}
