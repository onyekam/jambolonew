<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansAssociationExecutives2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_association_executives', function($table)
        {
            $table->boolean('leader')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_association_executives', function($table)
        {
            $table->dropColumn('leader');
        });
    }
}
