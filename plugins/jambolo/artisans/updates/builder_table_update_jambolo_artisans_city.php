<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansCity extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_city', function($table)
        {
            $table->integer('state_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_city', function($table)
        {
            $table->dropColumn('state_id');
        });
    }
}
