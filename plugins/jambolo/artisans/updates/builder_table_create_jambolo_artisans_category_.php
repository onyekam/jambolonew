<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloArtisansCategory extends Migration
{
    public function up()
    {
        Schema::create('jambolo_artisans_category_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('categoryid');
            $table->string('category_name', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_artisans_category_');
    }
}
