<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArea4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->integer('priority2')->nullable();
            $table->integer('priority3')->nullable();
            $table->integer('priority4')->nullable();
            $table->integer('priority5')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->dropColumn('priority2');
            $table->dropColumn('priority3');
            $table->dropColumn('priority4');
            $table->dropColumn('priority5');
        });
    }
}
