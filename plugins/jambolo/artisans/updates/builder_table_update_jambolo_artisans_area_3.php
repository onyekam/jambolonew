<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArea3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->renameColumn('area_id', 'id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_area', function($table)
        {
            $table->renameColumn('id', 'area_id');
        });
    }
}
