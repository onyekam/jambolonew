<?php namespace Jambolo\Artisans\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloArtisansArtisan12 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->decimal('latitude', 10, 7)->default(0.000000)->change();
            $table->decimal('longitude', 10, 7)->default(0.000000)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_artisans_artisan', function($table)
        {
            $table->decimal('latitude', 10, 7)->default(null)->change();
            $table->decimal('longitude', 10, 7)->default(null)->change();
        });
    }
}
