<?php namespace Jambolo\Artisans\Http;

use Backend\Classes\Controller;
use Jambolo\Artisans\Models\Artisan;

/**
 * Artisans A P I Controller Back-end Controller
 */
class ArtisansAPIController extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';


    public function index(){
    	$artisans = Artisan::all();
    	return $artisans;
    }

}
