<?php namespace Jambolo\Artisans;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
    		'Jambolo\Artisans\Components\HomeArtisanSearch' => 'HomeArtisanSearch',
            'Jambolo\Artisans\Components\AssociationArtisanSearch' => 'AssociationArtisanSearch',
    		'Jambolo\Artisans\Components\DisplaySearchResults' => 'DisplayArtisanSearch',
            'Jambolo\Artisans\Components\FooterCategories' => 'FooterCategories',
            'Jambolo\Artisans\Components\ArtisanDetails' => 'ArtisanDetails',
            'Jambolo\Artisans\Components\ArtisanRating' => 'Rating',
            'Jambolo\Artisans\Components\DisplayCommentsAndRatings' => 'DisplayCommentsAndRatings',
            'Jambolo\Artisans\Components\OverallRating' => 'OverallRating',
            'Jambolo\Artisans\Components\Associations' => 'Associations',
            'Jambolo\Artisans\Components\AssociationDetails' => 'AssociationDetails',
            'Jambolo\Artisans\Components\AssociationLeadership' => 'AssociationLeadership',
            'Jambolo\Artisans\Components\AssociationChairmen' => 'AssociationChairmen',
            'Jambolo\Artisans\Components\AssociationArtisans' => 'AssociationArtisans',
            'Jambolo\Artisans\Components\FullCategoryList' => 'FullCategoryList',
            'Jambolo\Artisans\Components\CategoryArtisans' => 'CategoryArtisans',
            'Jambolo\Artisans\Components\FeaturedCategories' => 'FeaturedCategories',
            'Jambolo\Artisans\Components\ArtisanDashboard' => 'ArtisanDashboard',
            'Jambolo\Artisans\Components\ArtisanLogin' => 'ArtisanLogin',
            'Jambolo\Artisans\Components\HomeArtisans' => 'HomeArtisans',
            'Jambolo\Artisans\Components\ContactForm' => 'ContactForm',
            'Jambolo\Artisans\Components\CategorySeo' => 'categorySEO',
            'Jambolo\Artisans\Components\AnalyticsDash' => 'analyticsdash',
            'Jambolo\Artisans\Components\Freepdf' => 'freepdf'

    	];
    }

    public function registerSettings()
    {
    }
}
