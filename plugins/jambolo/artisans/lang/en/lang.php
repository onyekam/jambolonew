<?php return [
    'plugin' => [
        'name' => 'Artisans',
        'description' => '',
    ],
    'artisan' => [
        'fname' => 'fname',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'phone_number' => 'Phone Number',
        'address' => 'Address',
        'latitude' => 'Latitude',
        'longitude' => 'Longitude',
        'state' => 'State',
        'city' => 'City',
        'area' => 'Area',
        'category' => 'Category',
        'premium' => 'Premium',
        'association' => 'Association',
        'manage_artisans' => 'Manage Artisans',
        'category_name' => 'Category Name',
        'association_name' => 'Association Name',
        'company_name' => 'Company Name',
        'slug' => 'Slug',
    ],
    'state' => [
        'name' => 'State Name',
        'slug' => 'Slug',
        'manage_states' => 'Manage States',
        'id' => 'State',
    ],
    'city' => [
        'name' => 'City Name',
        'slug' => 'Slug',
        'manage_cities' => 'Manage Cities',
    ],
    'area' => [
        'name' => 'Area Name',
        'slug' => 'Slug',
        'manage_areas' => 'Manage Areas',
    ],
    'association' => [
        'name' => 'Association Name',
        'slug' => 'Slug',
        'manage_association' => 'Manage Association',
    ],
    'artisancategory' => [
        'slug' => 'Slug',
        'name' => 'Category Name',
        'manage_category' => 'Manage Categories',
    ],
    'association_executives' => [
        'manage_executives' => 'Manage Executives',
    ],
    'zonalchairman' => [
        'manage_zonal_chairmen' => 'Manage Zonal Chairmen',
    ],
];