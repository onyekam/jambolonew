<?php namespace Jambolo\Artisans\Models;

use Form;
use Model;

/**
 * Model
 */
class City extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_city';

    public $belongsTo = [
        'state' => 'Jambolo\Artisans\Models\State'
    ];

    public $hasMany = [
        'areas' => 'Jambolo\Artisans\Models\Area'
    ];

    protected static $nameList = null;

    public static function getNameList()
    {
        if (self::$nameList) {
            return self::$nameList;
        }

        return self::$nameList = self::all()->lists('city_name', 'id');
    }

    public static function formSelect($name, $selectedValue = null, $options = []){
        return Form::select($name, $selectedValue, $options);
    }
}