<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class ParentCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_parent_categories';

    public $hasMany = [
        'categories' => 'Jambolo\Artisans\Models\ArtisanCategory',
    ];
}