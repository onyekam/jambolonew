<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class Area extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_area';

    public $belongsTo = [
        'city' => 'Jambolo\Artisans\Models\City',
        'zone' => 'Jambolo\Artisans\Models\Zone'
    ];

    public $hasMany = [
        'zonal_chairmen' => 'Jambolo\Artisans\Models\ZonalChairman'
    ];

    protected static $nameList = null;

    public static function getNameList($cityId)
    {
        if (isset(self::$nameList[$cityid])) {
            return self::$nameList[$cityid];
        }

        return self::$nameList[$cityid] = self::where('city_id', $cityid)->lists('area_name', 'id');
    }

    public static function formSelect($name, $countryId = null, $selectedValue = null, $options = []){
        return Form::select($name, self::getNameList($countryId), $selectedValue, $options);
    }
}