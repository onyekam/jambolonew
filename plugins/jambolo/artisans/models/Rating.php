<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class Rating extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = ['artisan_id', 'rating', 'comments'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_ratings_artisans';

    
    public $belongsTo = [
        'artisan' => 'Jambolo\Artisans\Models\Artisan',
        'user' => 'Rainlab\User\Models\User'
    ];

}