<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class Association extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_association';

    public $hasMany = [
        'artisan' => 'Jambolo\Artisans\Models\Artisan',
        'executives' => 'Jambolo\Artisans\Models\Executive',
        'zonal_chairmen' => 'Jambolo\Artisans\Models\ZonalChairman'
    ];

    public $attachOne = [
        'logo' => 'System\Models\File',
        'headerimage' => 'System\Models\File'
    ];

}