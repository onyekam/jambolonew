<?php namespace Jambolo\Artisans\Models;

use Backend\Models\ImportModel;

/**
 * BookImport Model
 */
class ArtisanImport extends ImportModel
{
    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = ['company_name' => 'required'];

    public function importData($results, $sessionKey = null)
    {
        foreach ($results as $row => $data) {
            try {
                // First create the Author if not exists
                $author_data = (!is_array($data['author'])) ? json_decode($data['author']) : $data['author'];
                $modelAuthor = null;
                if(is_array($author_data) && count($author_data)) {
                    $modelAuthor = Author::whereEmail($author_data['email']);
                    if(!$modelAuthor) {
                        $modelAuthor = Author::create(['name' => $author_data['name'], 'email' => author_data['email']]);
                        if(array_key_exists('contacts', $author_data)) {
                            foreach($author_data['contacts'] as $author_contact){
                                Contact::create(['name' => $author_contact['name'] , 'author_id' => $modelAuthor->id ]);
                            }
                        }
                    }
                }

                // Create/Update the Book model
                $book_data = [
                    'title' => $data['title'],
                    'description' => $data['description']
                ];
                if($modelAuthor) {
                    $book_data['author_id'] = $modelAuthor->id;
                }
                if(array_key_exists('id', $data)){
                    $book = Book::find($data['id']);
                    $book->update($book_data);
                } else {
                    $book = Book::create($book_data);
                }

                // Add categories
                $categories_data = (!is_array($data['categories'])) ? json_decode($data['categories']) : $data['categories'];
                if (is_array($categories_data) && count($categories_data)) {
                    $cd = collect($categories_data);
                    $cd->forget('pivot');
                    $cd->each(
                        function ($category) use ($book) {
                            $name = $category->name;
                            if (!$book->categories()->whereName($name)->count()) {
                                $modelCategory = Category::whereName($name)->first();
                                if (!$modelCategory) {
                                    $modelCategory = Category::create($category->toArray());
                                }
                                $book->categories()->attach($modelCategory->id);
                            }
                        }
                    );
                }

                if (array_key_exists('id', $data)) {
                    $this->logUpdated();
                } else {
                    $this->logCreated();
                }

            } catch (\Exception $ex) {
                $this->logError($row, $ex->getMessage());
            }
        }
    }
}