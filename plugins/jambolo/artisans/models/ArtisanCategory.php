<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class ArtisanCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_category_';

    public $attachOne = [
        'category_image' => 'System\Models\File'
    ];

    public $hasMany = [
        'categoryartisans' => ['Jambolo\Artisans\Models\Artisan', 'key' => 'category_id'],
    ];

    public $belongsTo = [
        'parent_category' => 'Jambolo\Artisans\Models\ParentCategory',
        'landing' => 'Jambolo\Customerinfo\Models\Landing'
    ];
}