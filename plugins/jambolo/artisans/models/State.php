<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class State extends Model
{
    use \October\Rain\Database\Traits\Validation;

    //protected $primaryKey = 'state_id';
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_state';

    public $hasMany = [
        'cities' => 'Jambolo\Artisans\Models\City'
    ];
}