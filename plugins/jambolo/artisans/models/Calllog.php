<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class Calllog extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_call_log';

    public $belongsTo = [
        'artisan' => 'Jambolo\Artisans\Models\Artisan'
    ];
}