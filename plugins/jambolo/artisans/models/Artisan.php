<?php namespace Jambolo\Artisans\Models;

use Model;

/**
 * Model
 */
class Artisan extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_artisans_artisan';


    public $hasMany = [
        'ratings' => 'Jambolo\Artisans\Models\Rating',
        'calllogs' => 'Jambolo\Artisans\Models\Calllog',
        'products' => 'Jambolo\Products\Models\Product',
        'order_details' => 'Jambolo\Products\Models\OrderDetails'
    ];

    public $attachOne = [
        'artisan_image' => 'System\Models\File'
    ];

    public $belongsTo = [
        'category' => 'Jambolo\Artisans\Models\ArtisanCategory',
        'area' => 'Jambolo\Artisans\Models\Area',
        'state' => 'Jambolo\Artisans\Models\State',
        'city' => 'Jambolo\Artisans\Models\City',
        'association' => 'Jambolo\Artisans\Models\Association',
        
    ];

    public $attachMany = [
        'premium_gallery' => ['System\Models\File','order'=>'sort_order']
    ];


}