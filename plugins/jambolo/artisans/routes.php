<?php

use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory as Category;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\Zone;
use RainLab\User\Models\User as UserModel;
use Vdomah\Excel\Classes\Excel;

//use Auth;

// Route::group(['prefix' => 'api/v1'], function () {
//     Route::resource('artisansapicontrollers', 'Jambolo\Artisans\Http\ArtisansAPIController');
//     //Route::resource('artisansapicontrollers', 'Jambolo\Artisans\Http\ArtisansAPIController');
//     //Route::resource('artisans', 'Jambolo\Artisans\Controllers\Artisans');
// });

Route::get('sitemap.xml', function(){
    return Response::view('jambolo.artisans::sitemap')->header('Content-Type', 'text/xml');
});

Route::get('robots.txt', function(){
    return Response::view('jambolo.artisans::robot')->header('Content-Type', 'text/plain');
});

Route::get('excelfileupload', function() {
    Excel::filter('chunk')->load(base_path() . '/storage/app/media/jamboloCleaned2.xlsx')->chunk(50, function($reader) {
                    
        foreach($reader as $reader){
            echo "<br>" . $reader['company_name'];
        }
        //dd($row);
                    //$results = $reader->get();
                   // dd($reader->excel());
                    //return $results;
                    //dd($results);
                    //dd($reader->first());
                    //return $reader;
                });    

});


// Route::get('api/v1/artisans', function () {
//     $artisans = Artisan::all();
//     return $artisans;
// })->middleware('jwt-auth');

// Route::get('api/v1/artisans/{phonenumber}', function ($phonenumber) {
//     $artisanWithNumber = Artisan::where('phone_number', $phonenumber)->first();
//     return $artisanWithNumber;
// })->middleware('jwt-auth');

// Route::post('test', function (\Request $request) {
//    return response()->json(('The test was successful'));
// })->middleware('jwt-auth');




// Route::get('api/v1/users', function () {
//     $users = User::all();
//     return $users;
// });

Route::get('api/v1/users/{phonenumber}', function ($phonenumber) {
    $userWithNumber = User::where('phone_number', $phonenumber)->first();
    return $userWithNumber;
});

Route::group(['prefix' => 'api/v1'], function() {

        Route::post('users', function (\Request $request) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $users = UserModel::all(['name','email','phone_number']);
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('token','users'));
        });

        Route::post('users/{phonenumber}', function (\Request $request, $userNumber) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            //$phonenumber = input('phonenumber');
             //dd($artisanNumber);
            $userWithNumber = UserModel::where('phone_number', $userNumber)->get(['name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('token','userWithNumber'));
        });

        Route::post('artisans', function (\Request $request) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $artisans = Artisan::all(['company_name','phone_number']);
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('token','artisans'));
        });

        Route::post('artisans/{phonenumber}', function (\Request $request, $artisanNumber) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('token','artisanWithNumber'));
        });

        Route::post('areas', function (\Request $request) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            //$category = Category::where('slug',$category)->first();
            $areas = Area::all(['id','slug']);
            //$categoryArtisansInArea = Artisan::where('category_id', $category->id)->where('area_id', $area->id)->get(['company_name', 'phone_number', 'latitude', 'longitude', 'address', 'premium']);
            //$artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->where('',$category)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('token','areas'));
        });


        // Klass Security Integration Endpoints

        Route::post('categories', function (\Request $request) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $categories = ParentCategory::all(['id','slug']);
            //$areas = Area::all()->get(['slug']);
            //$categoryArtisansInArea = Artisan::where('category_id', $category->id)->where('area_id', $area->id)->get(['company_name', 'phone_number', 'latitude', 'longitude', 'address', 'premium']);
            //$artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->where('',$category)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            // return  response()->json(compact('token','categories'));
            return  response()->json(compact('categories'));
        }); 
        
        
        Route::post('categories/{parentCategoryId}', function (\Request $request, $parentCategoryId) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password')
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $categories = Category::where('parent_category', $parentCategoryId)->get(['id','slug']);
            //$areas = Area::all()->get(['slug']);
            //$categoryArtisansInArea = Artisan::where('category_id', $category->id)->where('area_id', $area->id)->get(['company_name', 'phone_number', 'latitude', 'longitude', 'address', 'premium']);
            //$artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->where('',$category)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('categories'));
        });

         Route::post('zones', function (\Request $request) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password')
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $zones = Zone::all(['id','slug']);
            //$areas = Area::all()->get(['slug']);
            //$categoryArtisansInArea = Artisan::where('category_id', $category->id)->where('area_id', $area->id)->get(['company_name', 'phone_number', 'latitude', 'longitude', 'address', 'premium']);
            //$artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->where('',$category)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('zones'));
        });

         Route::post('zones/{zoneId}', function (\Request $request, $zoneId) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password')
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $areas = Area::where('zone_id', $zoneId)->get(['id','slug']);
            //$areas = Area::all()->get(['slug']);
            //$categoryArtisansInArea = Artisan::where('category_id', $category->id)->where('area_id', $area->id)->get(['company_name', 'phone_number', 'latitude', 'longitude', 'address', 'premium']);
            //$artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->where('',$category)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('areas'));
        });

        Route::post('artisans/{category}/{area}', function (\Request $request, $area, $category) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];

            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            $userModel = JWTAuth::authenticate($token);
            $user = [
                'id' => $userModel->id,
                'email' => $userModel->email,
                'name' => $userModel->name
            ];
            $category = Category::where('id',$category)->first();
            $area = Area::where('id',$area)->first();
            $categoryArtisansInArea = Artisan::where('category_id', $category->id)->where('area_id', $area->id)->take(10)->inrandomorder()->get(['company_name', 'phone_number', 'premium']);
            //$artisanWithNumber = Artisan::where('phone_number', $artisanNumber)->where('',$category)->get(['company_name','phone_number'])->first();
            
            //$users = User::all()->pluck('name', 'email');
            return  response()->json(compact('categoryArtisansInArea'));
        });


        // End of Klass Security Endpoints

         
    
        Route::post('login', function (\Request $request) {
            $credentials = [
                'email' => Request::get('email'),
                'password' => Request::get('password'),
            ];
    
            try {
                // verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
    
            $userModel = JWTAuth::authenticate($token);
    
            $user = [
                'id' => $userModel->id,
                'name' => $userModel->name,
                'surname' => $userModel->surname,
                'username' => $userModel->username,
                'email' => $userModel->email,
                'is_activated' => $userModel->is_activated,
            ];
            // if no errors are encountered we can return a JWT
            return response()->json(compact('token', 'user'));
        });
    
        Route::post('/signup', function () {
            $credentials = Input::only('email', 'password', 'password_confirmation');
    
            try {
                $user = UserModel::create($credentials);
            } catch (Exception $e) {
                return Response::json(['error' => $e->getMessage()], 401);
            }
    
            $token = JWTAuth::fromUser($user);
    
            return Response::json(compact('token', 'user'));
        });


        

        
});