<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;
use Rainlab\User\Models\User;

class HomeArtisans extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display artisans in the category',
			'description' => 'All artisans in the selected category will be displayed'
		];
	}

	public function onRun(){

		$this->categories = $this->loadHomeCategories();
		$this->artisans = $this->loadHomeArtisans();
		$this->getFacts();
		
	}

	public function searchQuery(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$this->category = $categoryObject;
		$searchQuery = Artisan::where('category_id', $categoryObject->id)->paginate(12);
		return $searchQuery;
	}

	public function loadHomeCategories(){
        $categories = ArtisanCategory::take(8)->inrandomorder()->get(); 
		return $categories;
    }
    
    public function loadHomeArtisans(){
        $artisans = Artisan::where('premium','1')->where('featured', '1')->take(6)->inrandomorder()->get();
        return $artisans;
	}
	
	public function getFacts(){
		$this->numberArtisans = count(Artisan::all());
		$this->numberCategories = count(ArtisanCategory::all());
		$this->numberAreas = count(Area::all());
		$this->numberUsers = count(User::all());
	}

	public $numberArtisans;
	public $numberUsers;
	public $numberCategories;
	public $numberAreas;
    public $artisans;
	public $categories;
	public $allCategoryArtisans;
	
	
}