<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;

class FeaturedCategories extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Featured Categories',
			'description' => 'Display Featured Categories in Homepage'
		];
	}

	public function onRun(){

		
		$this->carpenters = ArtisanCategory::where('slug','carpenters')->first();
		$this->architects = ArtisanCategory::where('slug','architect')->first();
		$this->aluminiumWorks = ArtisanCategory::where('slug','aluminium-works')->first();
		$this->artists = ArtisanCategory::where('slug','art-dealers-artist')->first();
		
	}


	public $carpenters;
	public $architects;
	public $artists;
	public $aluminiumWorks;
	
	
}