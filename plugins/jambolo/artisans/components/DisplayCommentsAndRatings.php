<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\Rating;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;

class DisplayCommentsAndRatings extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Comments and Ratings Display ',
			'description' => 'Display all artisan comments and ratings'
		];
	}

	public function onRun(){
		$this->artisan = $this->loadArtisan();
		//return $this->artisan;
		$this->ratingsAndComments = $this->displayArtisanCommentsAndRatings($this->artisan->id);
		if (count($this->displayArtisanCommentsAndRatings($this->artisan->id)) > 0) {
			$this->numberOfRatingsAndComments = count($this->displayArtisanCommentsAndRatings($this->artisan->id));
		} else {
			$this->numberOfRatingsAndComments = 1;
		}
		
		$average = [];
		$serviceRatings = [];
		$qualityRatings = [];
		$pricingRatings = [];
		foreach ($this->ratingsAndComments as $ratings) {
			array_push($average, ($ratings->rating_service + $ratings->rating_quality + $ratings->rating_pricing)/3);
		}

		foreach ($this->ratingsAndComments as $ratings) {
			array_push($serviceRatings, $ratings->rating_service);
		}

		foreach ($this->ratingsAndComments as $ratings) {
			array_push($qualityRatings, $ratings->rating_quality);
		}

		foreach ($this->ratingsAndComments as $ratings) {
			array_push($pricingRatings, $ratings->rating_pricing);
		}

		//return $average;
		//$this->overallAverage = array_sum($average)/$this->numberOfRatingsAndComments;
		//return $this->artisan->overall_average_rating;
		$this->overallAverage = $this->artisan->overall_average_rating;
		$this->serviceAverage = array_sum($serviceRatings)/$this->numberOfRatingsAndComments;
		$this->pricingAverage = array_sum($pricingRatings)/$this->numberOfRatingsAndComments;
		$this->qualityAverage = array_sum($qualityRatings)/$this->numberOfRatingsAndComments;

		//averages Rounded to be used to display stars
		$this->overallAverageRounded = round($this->overallAverage,0,PHP_ROUND_HALF_UP);
		$this->serviceAverageRounded = round(array_sum($serviceRatings)/$this->numberOfRatingsAndComments,0,PHP_ROUND_HALF_UP);
		$this->pricingAverageRounded = round(array_sum($pricingRatings)/$this->numberOfRatingsAndComments,0,PHP_ROUND_HALF_UP);
		$this->qualityAverageRounded = round(array_sum($qualityRatings)/$this->numberOfRatingsAndComments,0,PHP_ROUND_HALF_UP);



		//return $overallAverage;
		//return $this->numberOfRatingsAndComments;
		//return $this->resultsJSON;
	}

	public function displayArtisanCommentsAndRatings($artisanid){
		$artisanCommentsAndRatings = Rating::where('artisan_id',$artisanid)->get();
		return $artisanCommentsAndRatings;
	}

	public function loadArtisan(){
		return Artisan::where('slug',$this->param('slug'))->first();
	}
	public $artisan;
	public $artisanid;
	public $ratingsAndComments;
	public $numberOfRatingsAndComments;
	public $overallAverage;
	public $serviceAverage;
	public $pricingAverage;
	public $qualityAverage;
	public $overallAverageRounded;
	public $serviceAverageRounded;
	public $pricingAverageRounded;
	public $qualityAverageRounded;
	
}