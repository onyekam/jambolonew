<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;

use Jambolo\Artisans\Models\Executive;
use Jambolo\Artisans\Models\ZonalChairman;
use Jambolo\Artisans\Models\Association;


class AssociationChairmen extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Association Zonal Chairmen',
			'description' => 'Display an Association\'s Zonal Chairmen details'
		];
	}

	public function onRun(){
		$this->association = Association::where('slug',$this->param('slug'))->first();
		//return $this->association;
		//$this->executives = Executive::where('association_id',$this->association->id)->get();
		$this->zonalChairmen = ZonalChairman::where('association_id',$this->association->id)->get();
		//return $this->executives;
		//$this->relatedArtisans = Artisan::where('category_id',$this->categoryId)->take(4)->orderBy(Db::raw('RAND()'))->get();
		//return $this->relatedArtisans;
		//return $this->artisan;
	}
		public $association;


		public $zonalChairmen;
	
	
}