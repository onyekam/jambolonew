<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Jambolo\Artisans\Models\Rating;
use Illuminate\Support\Facades\Input;
use Redirect;

class ArtisanRating extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Rating',
			'description' => 'Rate an artisan'
		];
	}



	public function onRate() {
        $artisanid = post('artisan_id');
        $serviceValue = post('service');
        $qualityValue = post('quality');
        $pricingValue = post('pricing');
        $userid = post('user_id');
        $rating = new Rating;
        $rating->user_id = $userid;
        $rating->artisan_id = $artisanid;
        $rating->rating_service = $serviceValue;
        $rating->rating_quality = $qualityValue;
        $rating->rating_pricing = $pricingValue;
        $rating->comments = post('comments');
        $rating->save();
        //return Redirect::to($this->);

        $artisan = Artisan::find($artisanid);
        $artisanRatings = Rating::where('artisan_id',$artisanid)->get();
        if (count($artisanRatings) > 0) {
            $numberOfArtisanRatings = count($artisanRatings);
        } else {
            $numberOfArtisanRatings = 1;
        }
        
        $averageOfEachRating = [];
        foreach ($artisanRatings as $artisanRating) {
            array_push($averageOfEachRating, ($artisanRating->rating_service + $artisanRating->rating_quality + $artisanRating->rating_pricing)/3);
        }

        $overallAverageForArtisan = array_sum($averageOfEachRating)/$numberOfArtisanRatings;
        $artisan->overall_average_rating = $overallAverageForArtisan;
        $artisan->save(); 
        return redirect($this->currentPageUrl());
    }

}