<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Input;
//use Illuminate\Support\Facades\Input;
use Db;

class DisplaySearchResults extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Artisan Search Results',
			'description' => 'Display Artisans Search'
		];
	}

	public function onRun(){
		if (post('category')) {
			$this->category = post('category');
			if (post('area')) {
				$this->area = post('area');
				$this->areaName = Area::find(post('area'));
				//dd($this->area.' '.$this->areaName.' '.$this->category);
				//$this->results = $this->searchEngineRules($this->area, $this->category);
			} else {
				//$this->results = $this->searchCategoryById($this->category);
			}
			
			 $this->results = $this->searchQuery($this->area, $this->category);
			 if (empty($this->results)) {
			 	$this->alternateResults = $this->returnCategoryArtisansInOtherAreas($this->category);
			 }
			
		} elseif (post('phone_number')) { 
			$this->phone_number = post('phone_number');
			$this->results = $this->searchByNumber($this->phone_number);
		} elseif (post('association')){
			$this->association = post('association');
			$this->area = post('area');
			$this->results = $this->associationSearchQuery($this->area, $this->association);
		} 
		// elseif ($this->param('slug')) {
		// 	$this->results = $this->searchCategory($this->param('slug'));
		// }
		if (empty($this->results)) {
			$this->resultsCount = 0;
		} else {
			$this->resultsCount = count($this->results);
		}
		$this->randomCategories = $this->loadRandomCategories();
		$this->categoryObject = ArtisanCategory::find($this->category);
		$this->resultsJSON = json_encode($this->results);
	}

	public function searchCategory($slug){
		$category = Category::where('slug', $slug)->first();
		$searchQuery = Artisan::where('category_id', $category->id)->orderBy('premium', 'DESC')->get();
		return $searchQuery;
	}

	public function searchCategoryById($category){
		$searchQuery = Artisan::where('category_id', $category)->get();
		return $searchQuery;
	}

	public function searchByNumber($phone_number){
		$phone_number = substr($phone_number, 1);
		$searchQuery = Artisan::where('phone_number', 'LIKE' , '%'.$phone_number.'%')->get();
		return $searchQuery;
	}

	public function searchArea($area){
		$searchQuery = Artisan::where('area_id',$area)->get();
		return $searchQuery;
	}

	public function searchQuery($area, $category){
		$searchQuery = Artisan::where('area_id',$area)->where('category_id', $category)->orderBy('premium', 'DESC')->get();
		return $searchQuery;
	}

	public function returnCategoryArtisansInOtherAreas($category){
		//$searchQuery = Artisan::where('category_id', $category)->get();
		$searchQuery = Artisan::where('category_id', $category)->orderBy('premium', 'DESC')->orderBy(Db::raw('RAND()'))->get();
		return $searchQuery->take(16);
	}

	public function associationSearchQuery($area, $association){
		$searchQuery = Artisan::where('area_id',$area)->where('association_id', $association)->get();
		return $searchQuery;
	}

	public function loadRandomCategories(){
		return ArtisanCategory::take(5)->orderBy(Db::raw('RAND()'))->get();
	}

	public function searchEngineRules($areaId, $categoryId){
		$area = Area::where('id',$areaId)->first();
		//dd($area);
		$category = ArtisanCategory::where('id', $categoryId)->first();
		//dd($category);
		$requestedArea = Artisan::where('area_id',$areaId)->where('category_id', $categoryId)->orderBy('premium', 'DESC')->get();
		//dd($requestedArea);
		if (count($requestedArea) == 0) {
			if ($area->priority2) {
				$priorityArea = $this->getAreaIdbyId($area->priority2);
				$priority2 = Artisan::where('area_id',$priorityArea)->where('category_id', $categoryId)->orderBy('premium', 'DESC')->get();
				if (count($priority2) == 0) {
					if ($area->priority3) {
						$priorityArea = $this->getAreaIdbyId($area->priority3);
						$priority3 = Artisan::where('area_id', $priorityArea)->where('category_id', $categoryId)->orderBy('premium', 'DESC')->get();
						if (count($priority3) == 0) {
							if ($area->priority4) {
								$priorityArea = $this->getAreaIdbyId($area->priority4);
								$priority4 = Artisan::where('area_id',$priorityArea)->where('category_id', $categoryId)->orderBy('premium', 'DESC')->get();
								if (count($priority4) == 0) {
									if ($area->priority5) {
										$priorityArea = $this->getAreaIdbyId($area->priority5);
										$priority5 = Artisan::where('area_id',$this->getAreaIdById($area->priority5))->where('category_id', $categoryId)->orderBy('premium', 'DESC')->get();
										if (count($priority5) == 0) {
											$this->resultsMessage = "We haven't found any results matching your search";
										} else {
											$this->resultsMessage = "We couldn't find any ".$category->category_name." in ".$area->area_name.". Here are some ".$category->category_name." in ".$this->getAreaNameById($area->priority5);
											return $priority5;
										}
									} else {
										$this->resultsMessage = "We haven't found any results matching your search";
									}
									
								} else {
									$this->resultsMessage = "We couldn't find any ".$category->category_name." in ".$area->area_name.". Here are some ".$category->category_name." in ".$this->getAreaNameById($area->priority4) ;
									return $priority4;
								}
							} else {
								$this->resultsMessage = "We haven't found any results matching your search";
							}
							
						} else {
							$this->resultsMessage = "We couldn't find any ".$category->category_name." in ".$area->area_name.". Here are some ".$category->category_name." in ".$this->getAreaNameById($area->priority3) ;
							return $priority3;
						}
					} else {
						$this->resultsMessage = "We haven't found any results matching your search";
					}
					
				} else {
					$this->resultsMessage = "We couldn't find any ".$category->category_name." in ".$area->area_name.". Here are some ".$category->category_name." in ".$this->getAreaNameById($area->priority2) ;
					return $priority2;
				}
			} else {
				$this->resultsMessage = "We haven't found any results matching your search";
			}
			
			
		} else {
			$this->resultsMessage = "Here are ".$category->category_name." in ".$area->area_name.".";
			return $requestedArea;
		}
	}

	public function getAreaIdBySlug($areaSlug){
		$area = Area::where('slug',$areaSlug)->first();
		return $area->id;
	}

	public function getAreaNameById($areaId){
		$area = Area::where('id', $areaId)->first();
		return $area->area_name;
	}

	public function getAreaIdbyId($areaId){
		$area = Area::where('id',$areaId)->first();
		return $area->id;
	}
	
	public $resultsMessage;
	public $category;
	public $area;
	public $results;
	public $randomCategories;
	public $resultsCount;
	public $categoryObject;
	public $resultsJSON;
	public $association;
	public $alternateResults;
	public $areaName;
	public $phone_number;
	
}
