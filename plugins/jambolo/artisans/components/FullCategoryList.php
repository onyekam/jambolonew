<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;

class FullCategoryList extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display All Categories',
			'description' => 'Display Full List of Categories in Directory Page'
		];
	}

	public function onRun(){

		
		$this->allCategories = $this->loadAllCategories();
		$this->allPremiumCategories = $this->loadAllPremiumCategories();
		
	}

	public function searchQuery($area, $category){

		$searchQuery = Artisan::where('area_id',$area)->where('category_id', $category)->orderBy('premium', 'DESC')->get();
		return $searchQuery;
	}

	public function loadAllCategories(){
		return ArtisanCategory::all();
	}

	public function loadAllPremiumCategories(){
		return ArtisanCategory::whereHas('categoryartisans', function($query) {
			$query->where('premium','1');
		})->get();
	}


	public $allCategories;
	public $allPremiumCategories;
	
	
}