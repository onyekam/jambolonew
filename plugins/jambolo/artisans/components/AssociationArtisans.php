<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\Association;	
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Illuminate\Support\Facades\Input;
use Db;

class AssociationArtisans extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Association Artisans',
			'description' => 'Display Association Artisans'
		];
	}

	public function onRun(){
		$this->artisans = $this->loadArtisans();
		$this->artisansCount = count($this->loadArtisans2());
	}

	public function loadArtisans(){
		$association = Association::where('slug',$this->param('slug'))->first();
		$this->association = $association;
		return Artisan::where('association_id',$association->id)->paginate(12);
	}

	public function loadArtisans2(){
		$association = Association::where('slug',$this->param('slug'))->first();
		return Artisan::where('association_id',$association->id)->get();
	}

	public $artisans;
	public $artisansCount;
	public $association;
	
	
}