<?php namespace Jambolo\Artisans\Components;

//use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;
use Lang;
use Auth;
use Mail;
use Event;
use Flash;
//use Input;
use Request;
use Redirect;
use Validator;
use ValidationException;
use ApplicationException;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\User\Models\Settings as UserSettings;
use RainLab\User\Components\Account as RainLabUserComponents;
use Exception;
use Session;

class ArtisanLogin extends RainLabUserComponents {

	public function componentDetails(){
		return [
			'name' => 'Artisan Login',
			'description' => 'Artisan Login Page'
		];
	}

	public function onRun(){
		if (Session::has('loginArtisan')) {
			Session::keep('loginArtisan');
			return Redirect::to('/dashboard');	
		}
	}

	public function onSignin()
    {
        /*
         * Validate input
         */
        $data = post();
        $rules = [];

        $rules['login'] = $this->loginAttribute() == UserSettings::LOGIN_USERNAME
            ? 'required|between:2,64'
            : 'required|email|between:2,64';

        $rules['password'] = 'required|min:2';

        if (!array_key_exists('login', $data)) {
            $data['login'] = post('username', post('email'));
        }

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        /*
         * Authenticate user
         */
        $user = Auth::authenticate([
            'login' => array_get($data, 'login'),
            'password' => array_get($data, 'password')
        ], true);

        // your code here

        /*
         * Redirect to the intended page after successful sign in
         */
        $redirectUrl = $this->pageUrl($this->property('redirect'));

        if ($redirectUrl = post('redirect', $redirectUrl))
            return Redirect::intended($redirectUrl);
    }

    public function onArtisanSignin() {

    	$data = post();
    	//var_dump(post());
    	//return $data;
    	$rules = [];
    	$rules['phone_number'] = 'required';
    	$rules['password'] = 'required';
    	
    	// if(!array_key_exists('login',$data)){
    	// 	$data['login'] = post('phone_number');
    	// }


    	$validation = Validator::make($data, $rules);
    	if ($validation->fails()) {
    		throw new ValidationException($validation);
    	}
    	
    	// $artisan = Auth::authenticate([
    	//  	'phone_number' => array_get(post(), 'phone_number'),
    	//  	'password' => array_get(post(), 'password') 
    	//  ], true);

    	$credentials = [
    		'phone_number' 	=> array_get(post(), 'phone_number'),
            'password' 		=> array_get(post(), 'password')
		];
		//echo $credentials;
		


    	// $this->findLoginArtisan($credentials);
    	// var_dump($this->loginArtisan);
    	$loginArtisan = Artisan::where('phone_number', $credentials['phone_number'])->first();
    	if ($loginArtisan->premium == 1 && $loginArtisan->password == '') {
    		if ($credentials['password'] == '0000') {
				//return Redirect true;
				//return "hello I'm here";
				//dd();
				//$artisanDashboard = $this->property('artisanDashboard');
				
				//$artisanDashboardWithParams = $this->controller->pageUrl($artisanDashboard, $loginArtisan);
				//return $artisanDashboardWithParams;
				//dd();
				//Session::put('loginArtisan', json_encode($loginArtisan));
				return Redirect::to('/my-account')->with('loginArtisan' , json_encode($loginArtisan));
    		} else {
    			return false;
    		}
    	} elseif ($loginArtisan->premium == 1 && $loginArtisan->password != '') {
    		if ($credentials['password'] == $loginArtisan->password && $credentials['phone_number'] == $loginArtisan->phone_number) {
    			return Redirect::to('/my-account')->with('loginArtisan', json_encode($loginArtisan));
    		} else {
    			return false;
    		}
    	} else {
    		return "You need to be a premium artisan to be able to login";
    	}



    	
    }
// 2349030542226
    public function findLoginArtisan($credentials) {
    	$loginArtisan = Artisan::where('phone_number', $credentials['phone_number'])->first();
    	if ($loginArtisan->premium == 1 && $loginArtisan->password == '') {
    		if ($credentials['password'] == '0000') {
    			return true;
    		} else {
    			return false;
    		}
    	} elseif ($loginArtisan->premium == 1 && $loginArtisan->password != '') {
    		if ($credentials['password'] == $loginArtisan->password && $credentials['phone_number'] == $loginArtisan->phone_number) {
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		return "You need to be a premium artisan to be able to login";
    	}
    	//return $this->loginArtisan;

    }


    public $loginArtisan;






















	
	
}