<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;

class CategoryArtisans extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display artisans in the category',
			'description' => 'All artisans in the selected category will be displayed'
		];
	}

	public function onRun(){

		
		$this->allCategoryArtisans = $this->searchQuery();
		$this->everyCategoryArtisan = $this->searchQuery2();
		$this->everyCategoryArtisan2 = $this->searchQuery3();
		$this->premiumArtisans = $this->premiumSearchQuery();
		
	}

	public function searchQuery(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$this->category = $categoryObject;
		$this->resultCount =  count(Artisan::where('category_id', $categoryObject->id)->get());
		$searchQuery = Artisan::where('category_id', $categoryObject->id)->orderBy('premium', 'DESC')->paginate(12);
		return $searchQuery;
	}

	public function premiumSearchQuery(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$this->category = $categoryObject;
		$this->resultCount =  count(Artisan::where('category_id', $categoryObject->id)->where('premium','1')->get());
		$searchQuery = Artisan::where('category_id', $categoryObject->id)->where('premium','1')->paginate(12);
		return $searchQuery;
	}

	public function searchQuery2(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$this->resultCount =  count(Artisan::where('category_id', $categoryObject->id)->get());
		$searchQuery = Artisan::where('category_id', $categoryObject->id)->orderBy('premium', 'DESC')->get();
		return $searchQuery;
	}

	public function searchQuery3(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$this->resultCount =  count(Artisan::where('category_id', $categoryObject->id)->where('premium','1')->get());
		$searchQuery = Artisan::where('category_id', $categoryObject->id)->where('premium','1')->get();
		return $searchQuery;
	}

	public function loadAllCategories(){
		return ArtisanCategory::all();
	}

	public $category;
	public $allCategoryArtisans;
	public $everyCategoryArtisan;
	public $everyCategoryArtisan2;
	public $resultCount;
	public $premiumArtisans;
	
	
}