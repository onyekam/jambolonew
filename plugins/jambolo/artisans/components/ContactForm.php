<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Jambolo\Artisans\Models\Association;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Support\Facades\Mail;
use Validator;
use ValidationException;
use Flash;

class ContactForm extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Contact Form',
			'description' => 'Jambolo Contact Form'
		];
	}



	public function onRun(){
        //$this->associations = Association::all();
        //$this->artisan = $this->loadArtisan();
        //return $associations;
    }


    public function onSend(){

        $data = post();

        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'content' => 'required'
        ];
        
    	

        $validation = Validator::make($data, $rules);

        if ($validation->fails()) {
        throw new ValidationException($validation);
    } else {
        $vars = ['name' => Input::get('name'), 'email' => Input::get('email'), 'content' => Input::get('content'), 'subject' => Input::get('subject')];
         Mail::send('jambolo.artisans::mail.message', $vars, function($message) {

            $message->to('support@jambolo.com', 'Jambolo Admin');
            $message->subject('New message from contact form');

        });

        Flash::success('Message sent!');
    }

       

        //$artisan = Artisan::where('slug', $this->param('slug'))->first();

    	//return $artisan;
    }

   

}