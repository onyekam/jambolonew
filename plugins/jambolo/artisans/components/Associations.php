<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Jambolo\Artisans\Models\Association;
use Illuminate\Support\Facades\Input;
use Redirect;

class Associations extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Associations',
			'description' => 'List all associations'
		];
	}



	public function onRun(){
        $this->associations = Association::all();

    }
    public $associations;

}