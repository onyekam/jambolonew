<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\Area;
use Illuminate\Support\Facades\Input;
use BackendAuth;
use Db;

class AnalyticsDash extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display category SEO',
			'description' => 'SEO Fields for categories'
		];
	}

	public function onRun(){
		$this->categories = $this->loadAllCategories();
		$this->category = $this->searchQuery();
		$this->backendUser = BackendAuth::getUser();
	}

	public function searchQuery(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$category = $categoryObject;
		return $category;
	}

	public function searchQuery2(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$this->resultCount =  count(Artisan::where('category_id', $categoryObject->id)->get());
		$searchQuery = Artisan::where('category_id', $categoryObject->id)->get();
		return $searchQuery;
	}

	public function onCategory(){
		$categorySlugFromForm = post('category');
		$areas = [];
		$categorySlugFromForm;
		$artisanCountByArea['area'] = [];
		$artisanCountByArea['count'] = [];
		$category = ArtisanCategory::where('slug', $categorySlugFromForm)->first();
		$artisansInCategory = Artisan::where('category_id', $category->id)->get();
		//return $artisansInCategory;
		foreach ($artisansInCategory as $artisan) {
			$area = Area::where('id', $artisan->area_id)->first();
			array_push($areas, $area->area_name);
		}
		array_unique($areas);
	
		// foreach ($areas as $area) {
		// 	$areaObject = Area::where('area_name', $area)->first();
		// 	$artisanCountByArea[$area] = count(Artisan::where('category_id', $category->id)->where('area_id', $areaObject->id)->get());
		// }

		foreach ($areas as $area) {
			$areaObject = Area::where('area_name', $area)->first();
			array_push($artisanCountByArea['area'],$areaObject->area_name);
			//$artisanCountByArea['area'] = $areaObject->area_name;
			array_push($artisanCountByArea['count'],count(Artisan::where('category_id', $category->id)->where('area_id', $areaObject->id)->get()));
			//$artisanCountByArea['count'] = count(Artisan::where('category_id', $category->id)->where('area_id', $areaObject->id)->get());
		}
		//$result = array_combine(array_unique($artisanCountByArea['area']), array_unique($artisanCountByArea['count']));
		
		//$this->artisanCountByArea = $artisanCountByArea;
		return [
        	'#categoryinfo' => $this->renderPartial('@categoryinfo', ['category' => $category, 'artisanCountByArea' => array_combine(array_unique($artisanCountByArea['area']), array_unique($artisanCountByArea['count']))])
    	];
		
	}

	public function CategoryArtisansInArea(){

	}

	public function loadAllCategories(){
		return ArtisanCategory::all();
	}

	public $category;
	public $categories;
	public $allCategoryArtisans;
	public $everyCategoryArtisan;
	public $resultCount;
	public $backendUser;
	public $artisansInCategory;
	public $artisanCountByArea;
}