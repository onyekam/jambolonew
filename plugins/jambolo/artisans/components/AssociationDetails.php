<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\Association;
use Jambolo\Artisans\Models\Executive;
use Illuminate\Support\Facades\Input;

class AssociationDetails extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Association Details',
			'description' => 'Displays basic association details on association page'
		];
	}

	public function onStart(){
		
	}

	public function onRun(){
		$this->association = Association::where('slug',$this->param('slug'))->first();
		$this->associationLeader = Executive::where('leader','1')->where('association_id',$this->association->id)->first();
	}
	public $association;
	public $associationLeader;
	
}