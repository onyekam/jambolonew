<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;

class OverallRating extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Overall Artisan Rating',
			'description' => 'Display Overall Artisan Rating'
		];
	}
		
}