<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Jambolo\Artisans\Models\Calllog;
use Illuminate\Support\Facades\Input;
use Db;

class ArtisanDetails extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Artisan Details',
			'description' => 'Display an Artisan\'s details'
		];
	}

	public function onRun(){

		$this->artisan = $this->loadArtisan();
//		return $this->artisan->artisan_image;
		$this->categoryId = $this->artisan->category->id;
		$this->relatedArtisans = Artisan::where('category_id',$this->categoryId)->take(4)->orderBy(Db::raw('RAND()'))->get();
		//return $this->relatedArtisans;
		//return $this->artisan;
	}

	

	public function loadArtisan(){
		if(isset($_SERVER['HTTP_REFERER'])) {
			$this->prev= $_SERVER['HTTP_REFERER'];
			//dd($this->prev);
		}
		else {
			//echo "You must navigate from index.php page";
		}

		return Artisan::where('slug',$this->param('slug'))->first();
	}

	public function onCountCall(){
		$artisan = Artisan::where('id', post('id'))->first();
		$artisan->call_counter++;
		$artisan->save();
		$calllog = new Calllog;
		$calllog->artisan_id = $artisan->id;
		$calllog->save();

		//Redirect::to();
		header('location: tel:'.$artisan->phone_number);
	}

	public $artisan;
	public $relatedArtisans;
	public $categoryId;
	public $prev;
	
}