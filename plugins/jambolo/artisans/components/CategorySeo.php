<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\ArtisanCategory;
use Illuminate\Support\Facades\Input;
use Db;

class CategorySeo extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display category SEO',
			'description' => 'SEO Fields for categories'
		];
	}

	public function onRun(){
		$this->category = $this->searchQuery();
	}

	public function searchQuery(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$category = $categoryObject;
		return $category;
	}

	public $category;
	
}