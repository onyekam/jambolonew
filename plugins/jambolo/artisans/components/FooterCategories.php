<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Db;

class FooterCategories extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Random Categories',
			'description' => 'Display Random Categories in Footer'
		];
	}

	public function onRun(){

		
		$this->randomCategories = $this->loadRandomCategories();
		
	}

	public function searchQuery($area, $category){

		$searchQuery = Artisan::where('area_id',$area)->where('category_id', $category)->get();
		return $searchQuery;
	}

	public function loadRandomCategories(){
		return ArtisanCategory::take(5)->orderBy(Db::raw('RAND()'))->get();
	}


	public $randomCategories;
	
	
}
