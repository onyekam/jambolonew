<?php namespace Jambolo\Artisans\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Jambolo\Artisans\Models\Association;
use Illuminate\Support\Facades\Input;

class AssociationArtisanSearch extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Association Artisan Search',
			'description' => 'Search Artisans on Associations Page'
		];
	}

	public function onStart(){
		 $this->addCss('../../../themes/jambolo/assets/css/master.css');
	}

	public function onRun(){
		$this->cities = $this->loadCities();
		$this->areas = $this->loadAreas();
		$this->states = $this->loadStates();
		$this->association = Association::where('slug', $this->param('slug'))->first();
		//return $this->association;
		//$this->artisanCategories = $this->loadArtisanCategories();
		 $this->addCss('../../../themes/jambolo/assets/css/master.css');
	}

	protected function loadSearchResults(){

	}

	protected function loadCities(){
		return City::all();
	}

	protected function loadStates(){
		return State::all();
	}

	protected function loadAreas(){
		return Area::all();
	}

	// protected function loadArtisanCategories(){
	// 	return ArtisanCategory::all();
	// }

	public function onChooseCity(){
		//$valueInField = post('city.id');
		
		$city = post('city');
		$areasInCity = Area::where('city_id', $city)->get(); 
		//$a = post('cityId');

		//$areasInCity = City::all(); 

		//return $this->areasInCity;
		
		   return ['#areaDropdown2' => $this->renderPartial('@arearesults', [
		    	'areasInCity' => $areasInCity
		    ])];

		 
	}

	public function onSearch(){
		//$city = post('city');
		$area = post('area');
		$category = post('category');

		$area2 = Input::get('city');
		$category2 = Input::get('category');
		
		//Session::put('category', $category);

		//dd($category);
		//return $category;

		//$searchQuery = Artisan::where('area_id',$area)->where('category_id', $category)->get();

		//dd($searchQuery);

	}
	public $areasInCity;

	public $cities;
	public $areas;
	public $states;
	public $artisanCategories;
	public $association;
}