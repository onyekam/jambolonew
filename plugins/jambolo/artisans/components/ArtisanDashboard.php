<?php namespace Jambolo\Artisans\Components;
use Cms\Classes\ComponentBase;
use Jambolo\Artisans\Models\Artisan as Artisan;
use Jambolo\Artisans\Models\ArtisanCategory;
use Jambolo\Artisans\Models\Area;
use Jambolo\Artisans\Models\City;
use Jambolo\Artisans\Models\State;
use Illuminate\Support\Facades\Input;
use Redirect;
use System\Models\File as File;
use Session;
use Validator;
use ValidationException;
use Flash;
use Debugbar;
use Auth;
class ArtisanDashboard extends ComponentBase {
	public function componentDetails(){
		return [
			'name' => 'Artisan Dashboard',
			'description' => 'Artisan Dashboard Component'
		];
	}
	public function onRun(){
		/* $artisan = json_decode(Session::get('loginArtisan'),true);
		if (Session::has('loginArtisan')) {
			
			$sessionValue = json_decode(Session::get('loginArtisan'),true);
			$this->loggedInArtisan = $this->loadArtisan($sessionValue['slug']);
			
			Session::put('loginArtisan', json_encode($this->loggedInArtisan));
			Session::keep('loginArtisan');	
		} elseif(post('loginArtisan')) {
			$sessionValue = json_decode(post('loginArtisan'));
			$this->loggedInArtisan = $this->loadArtisan($sessionValue['slug']);
			Session::put('loginArtisan', json_encode($this->loggedInArtisan));
			Session::keep('loginArtisan');
			
		} else {
			return Redirect::to('/artisanlogin')->with('message','You must be logged in to access this page');
		}
		$this->getVars(); */
		$this->user = $user = Auth::getUser();
		// if (!($user->artisan_id)){
		// 	return Redirect::to('/');
		// }
		
		$this->loggedInArtisan = $this->loadArtisan($user->artisan_id);
		$this->getVars();
	}
	

    public function loadArtisan($id){
		$artisan = Artisan::where('id', $id)->first();
		$this->artisan = Artisan::where('id', $id)->first();
    	return $artisan;
	}
	public function getVars(){
		$this->categories = ArtisanCategory::all();
		$this->areas = Area::all();
		$this->cities = City::all();
		$this->states = State::all();
	}
	
	public function onImageUpload(){
		$artisanToUpdate = Artisan::where('slug',Input::get('slug'))->first();
		Session::put('loginArtisan', json_encode($artisanToUpdate));
		Session::keep(['loginArtisan']);
		$image = Input::all();
        $file = (new File())->fromPost($image['artisan_image']);
         return [
             '#artisan_image' => '<img src="'. $file->getThumb(200, 200, ['mode' => 'crop']).'">'
		 ];
	}
	public function onGalleryUpload(){
		$artisanToUpdate = Artisan::where('slug',Input::get('slug'))->first();
		Session::put('loginArtisan', json_encode($artisanToUpdate));
		Session::keep(['loginArtisan']);
		$image = Input::all();
		$files = [];
		$galleryDisplay = "";
		
		//return Debugbar::info($image);
		//return Debugbar::info($image['premium_gallery']);
		Debugbar::info($image);
		foreach ($image['premium_gallery'] as $galleryImage) {
			//Debugbar::info((new File())->fromPost($galleryImage));
			array_push($files, (new File())->fromPost($galleryImage));
		}
		//return $files;

		//Debugbar::info($files);
		foreach ($files as $item) {
			$galleryDisplay .= '<img src="'. $item->getThumb(200, 200, ['mode' => 'crop']).'" style="padding:2px;"/>';	
		}

		Debugbar::info($galleryDisplay);
		//return $galleryDisplay;
	   //return Debugbar::info($files);
		// $file = (new File())->fromPost($image['premium_gallery']);
         return [
             '#premium_gallery' => $galleryDisplay
		 ];
    }
	public function onUpdate(){
		// $data = post();
        // $rules = [
		
		// ];
        // $validation = Validator::make($data, $rules);

        // if ($validation->fails()) {
        // 	throw new ValidationException($validation);
    	// } else {
			
		$artisanToUpdate = Artisan::where('slug', Input::get('slug'))->first();
		//dd($artisanToUpdate);
		$artisanToUpdate->company_name = Input::get('company_name');
		$artisanToUpdate->first_name = Input::get('first_name');
		$artisanToUpdate->last_name = Input::get('last_name');
		$artisanToUpdate->address = Input::get('address');
		$artisanToUpdate->description = Input::get('description');
		$artisanToUpdate->phone_number = Input::get('phone_number');
		$artisanToUpdate->category_id = Input::get('category_id');
		$artisanToUpdate->area_id = Input::get('area_id');
		$artisanToUpdate->artisan_image = Input::file('artisan_image');
		$artisanToUpdate->premium_gallery = Input::file('premium_gallery');
		$artisanToUpdate->save();
		Flash::success('Listing updated');
		Session::put('loginArtisan', json_encode($artisanToUpdate));
		Session::keep(['loginArtisan']);

    	// }
	}

	

	public $loggedInArtisan;
	public $artisan;
	public $categories;
	public $areas;
	public $states;
	public $cities;
	public $user;
}