<?php namespace Jambolo\Ads;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Jambolo\Ads\Components\Displayads' => 'DisplayAds',
            'Jambolo\Ads\Components\Displayfeatures' => 'displayfeatures'
    	];
    }

    public function registerSettings()
    {
    }
}
