<?php namespace Jambolo\Ads\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Ads\Models\Ads;
use Db;


class Displayads extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Ads',
			'description' => 'Manage Ads'
		];
	}

	public function onRun(){
		$this->topAd = $this->topAds();
		$this->rightAd = $this->rightAds();
	}

	public function searchQuery(){
		$categoryObject = ArtisanCategory::where('slug', $this->param('slug'))->first();
		$category = $categoryObject;
		return $category;
    }
    
    public function topAds(){
        $ad = Ads::where('ad_position','top')->where('active',1)->take(1)->inrandomorder()->first();
        return $ad;
    }

    public function rightAds(){
        $ad = Ads::where('ad_position','right')->where('active',1)->take(1)->inrandomorder()->first();
        return $ad;
    }


	public $topAd;
	public $rightAd;
}