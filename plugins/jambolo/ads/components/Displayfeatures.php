<?php namespace Jambolo\Ads\Components;

use Cms\Classes\ComponentBase;
use Jambolo\Ads\Models\Ads;
use Db;
use Jambolo\Ads\Models\ProductFeatures;
use Jambolo\Ads\Models\MPSlide;

class Displayfeatures extends ComponentBase {

	public function componentDetails(){
		return [
			'name' => 'Display Features',
			'description' => 'Manage Features'
		];
	}

	public function onRun(){
		$this->featureOne = $this->getFeatureOne();
		$this->featureTwo = $this->getFeatureTwo();
		$this->topSlides = $this->getTopSlides();
		$this->midSlides = $this->getMidSlides();
	}

	public function getFeatureOne(){
		$featureOne = ProductFeatures::where('id',1)->first();
		return $featureOne;
	}
	
	public function getFeatureTwo(){
		$featureTwo = ProductFeatures::where('id',2)->first();
		return $featureTwo;
	}

	public function getTopSlides(){
		$slides = MPSlide::where('position_id',1)->get();
		return $slides;
	}

	public function getMidSlides(){
		$slides = MPSlide::where('position_id',2)->get();
		return $slides;
	}

	public $topSlides;
	public $midSlides;
	public $featureOne;
	public $featureTwo;
}