<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloAdsMpsliders extends Migration
{
    public function up()
    {
        Schema::create('jambolo_ads_mpsliders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('link')->nullable();
            $table->string('button_label')->nullable();
            $table->integer('postiion_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_ads_mpsliders');
    }
}
