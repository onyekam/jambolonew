<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloAdsPosition extends Migration
{
    public function up()
    {
        Schema::create('jambolo_ads_position', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('position_name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_ads_position');
    }
}
