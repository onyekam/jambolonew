<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloAdsAds extends Migration
{
    public function up()
    {
        Schema::create('jambolo_ads_ads', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_ads_ads');
    }
}
