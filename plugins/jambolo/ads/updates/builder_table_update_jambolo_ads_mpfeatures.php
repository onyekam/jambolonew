<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAdsMpfeatures extends Migration
{
    public function up()
    {
        Schema::table('jambolo_ads_mpfeatures', function($table)
        {
            $table->integer('product_id')->nullable(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_ads_mpfeatures', function($table)
        {
            $table->integer('product_id')->nullable()->change();
        });
    }
}
