<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAdsAds2 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_ads_ads', function($table)
        {
            $table->string('ad_position');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_ads_ads', function($table)
        {
            $table->dropColumn('ad_position');
        });
    }
}
