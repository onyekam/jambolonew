<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAdsMpsliders extends Migration
{
    public function up()
    {
        Schema::table('jambolo_ads_mpsliders', function($table)
        {
            $table->increments('id')->nullable(false)->unsigned(false)->default(null)->change();
            $table->renameColumn('postiion_id', 'position_id');
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_ads_mpsliders', function($table)
        {
            $table->increments('id')->nullable(false)->unsigned()->default(null)->change();
            $table->renameColumn('position_id', 'postiion_id');
        });
    }
}
