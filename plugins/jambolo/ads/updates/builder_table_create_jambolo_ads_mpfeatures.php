<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloAdsMpfeatures extends Migration
{
    public function up()
    {
        Schema::create('jambolo_ads_mpfeatures', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_ads_mpfeatures');
    }
}
