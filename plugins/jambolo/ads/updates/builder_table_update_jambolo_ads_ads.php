<?php namespace Jambolo\Ads\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAdsAds extends Migration
{
    public function up()
    {
        Schema::table('jambolo_ads_ads', function($table)
        {
            $table->boolean('active')->default(0);
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_ads_ads', function($table)
        {
            $table->dropColumn('active');
            $table->increments('id')->unsigned()->change();
        });
    }
}
