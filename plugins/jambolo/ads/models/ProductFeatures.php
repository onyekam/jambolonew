<?php namespace Jambolo\Ads\Models;

use Model;

/**
 * Model
 */
class ProductFeatures extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_ads_mpfeatures';

    public $belongsTo = [
        'product' => ['Jambolo\Products\Models\Product']
    ];

    public $attachOne = [
        'featureimg' => 'System\Models\File'
    ];
}