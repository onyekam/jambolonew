<?php namespace Jambolo\Ads\Models;

use Model;

/**
 * Model
 */
class MPSlide extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_ads_mpsliders';

    public $belongsTo = [
        'position' => ['Jambolo\Ads\Models\Position']
    ];

    public $attachOne = [
        'slider_image' => ['System\Models\File']
    ];
}