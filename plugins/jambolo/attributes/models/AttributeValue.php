<?php namespace Jambolo\Attributes\Models;

use Model;

/**
 * Model
 */
class AttributeValue extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];
    protected $fillable = ['price', 'attributevalue', 'attributes'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_attributes_values';

    // public $hasMany = [
       
    // ];

    public $belongsTo = [
        //'products' => ['Jambolo\Products\Models\Product', 'key' =>'product_id', 'other_key' => 'id'],
        'attribute' => ['Jambolo\Attributes\Models\Attribute', 'key' => 'attribute_id', 'other_key' => 'id']
    ];

    public $belongsToMany = [
        'productattributes' => 'Jambolo\Products\Models\ProductAttribute',
    ];
}