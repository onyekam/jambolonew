<?php namespace Jambolo\Attributes\Models;

use Model;

/**
 * Model
 */
class Attribute extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'jambolo_attributes_attributes';


    public $HasMany = [
        'attributevalues' => 'Jambolo\Attributes\Models\AttributeValue',
    ];

    public $BelongsToMany = [
        'products' => ['Jambolo\Products\Models\Product'],
            
    ];

}