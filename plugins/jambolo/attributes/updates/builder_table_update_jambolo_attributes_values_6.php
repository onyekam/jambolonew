<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAttributesValues6 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->string('attributevalue', 255)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->integer('attributevalue')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
