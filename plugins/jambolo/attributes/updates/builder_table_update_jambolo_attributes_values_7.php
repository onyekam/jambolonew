<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAttributesValues7 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
}
