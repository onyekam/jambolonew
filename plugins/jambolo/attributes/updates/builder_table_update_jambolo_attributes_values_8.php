<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAttributesValues8 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('attribute_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->integer('attribute_id')->nullable(false)->change();
        });
    }
}
