<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloAttributesValues extends Migration
{
    public function up()
    {
        Schema::create('jambolo_attributes_values', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('attribute_id');
            $table->string('value');
            $table->integer('product_id')->nullable();
            $table->decimal('price', 10, 0)->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_attributes_values');
    }
}
