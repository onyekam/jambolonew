<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateJamboloAttributesAttributes extends Migration
{
    public function up()
    {
        Schema::create('jambolo_attributes_attributes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('jambolo_attributes_attributes');
    }
}
