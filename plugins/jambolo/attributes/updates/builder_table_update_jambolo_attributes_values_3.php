<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAttributesValues3 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->integer('value')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->string('value', 255)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
