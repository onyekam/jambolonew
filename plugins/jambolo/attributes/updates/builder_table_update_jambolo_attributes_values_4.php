<?php namespace Jambolo\Attributes\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateJamboloAttributesValues4 extends Migration
{
    public function up()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->integer('value')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('jambolo_attributes_values', function($table)
        {
            $table->integer('value')->nullable(false)->change();
        });
    }
}
